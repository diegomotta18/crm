<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 25/10/17
 * Time: 12:31
 */

Route::get('users/index', 'UsersController@index' )->name('users.index');
Route::post('users/store','UsersController@store')->name('users.store');
Route::get('users','UsersController@getUsers');
Route::get('users/create','UsersController@create')->name('users.create');
Route::put('users/{id}/update','UsersController@update')->name('users.update');
Route::delete('users/{id}','UsersController@delete')->name('users.delete');
Route::get('users/{id}/edit','UsersController@edit')->name('users.edit');
Route::get('users/{id}','UsersController@show')->name('users.show');
