<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 25/10/17
 * Time: 12:31
 */

Route::get('/home', 'HomeController@index')->name('home');
Route::get('positions/list', 'PositionController@getPositions');
Route::get('positions/all', 'PositionController@getAllPosition');
Route::get('companies/list', 'CompanyController@getCompanies');
Route::get('companies/all', 'CompanyController@getAllCompany');
Route::get('items/list', 'ItemController@getItems');
Route::get('items/all', 'ItemController@getAllItem');
Route::get('categories/list', 'CategoryController@getCategory');
Route::get('categories/all', 'CategoryController@getAllCategory');
Route::get('type_clients/all', 'TypeClientController@getTypeClientAll');
Route::get('clients/index', 'ClientController@index')->name('clients.index');
Route::get('clients/createc','ClientController@changeForm')->name('clients.createc');
Route::post('clients/editChangeForm', 'ClientController@editChangeForm')->name('clients.editChangeForm');
Route::post('clients/storeWithoutCompany', 'ClientController@storeWithoutCompany')->name('clients.storeWithoutCompany');
Route::get('clients/{id}/{type_client}/edit', 'ClientController@edit')->name('clients.edit');
Route::put('clients/{id}/updateWithoutCompany', 'ClientController@updateWithoutCompany')->name('clients.updateWithoutCompany');
Route::get('clients', 'ClientController@getAllClients');
Route::get('clients/create', 'ClientController@create')->name('clients.create');
Route::post('clients/store', 'ClientController@store')->name('clients.store');
Route::put('clients/{id}/update', 'ClientController@update')->name('clients.update');
Route::get('clients/{id}/edit', 'ClientController@edit')->name('clients.edit');
Route::delete('clients/{id}', 'ClientController@destroy')->name('clients.destroy');
Route::get('clients/{id}', 'ClientController@show')->name('clients.show');
Route::get('clients/type/{type_client}', 'ClientController@getClientsForType')->name('clients.fortype');
Route::post('client/importfile','ClientController@importFile')->name('import');
Route::get('provincess/{id}', 'ProvinceController@getProvinces');
Route::get('districs/{id}', 'DistrictController@getDistricts');
Route::get('places/{id}', 'PlaceController@getPlaces');
Route::get('exports', 'ExportController@index')->name('export.index');
Route::post('exports', 'ExportController@filter')->name('filter');
Route::get('exporting', 'ExportController@export')->name('exporting');
Route::get('usersdt', 'UserController@getUsers')->name('users.dt');
Route::get('rolesdt', 'RolController@getRoles')->name('roles.dt');
Route::get('permissionsdt', 'PermissionController@getPermissions')->name('permissions.dt');
Route::get('typeclientsdt', 'TypeClientController@getTypes')->name('typeclient.dt');
Route::get('roles/{id}/permisos', 'RolController@getPermissions')->name('roles.permissions');
Route::put('roles/{id}/updpermisos', 'RolController@updatePermissions')->name('roles.updpermissions');
Route::get('countriesdt', 'CountryController@getCountries')->name('countries.dt');
Route::get('provincesdt/{country_id}', 'ProvinceController@getProvinces')->name('provinces.dt');
Route::get('districtsdt/{province_id}', 'DistrictController@getDistricts')->name('districts.dt');

Route::get('provincesj/{country_id}', 'ProvinceController@getProvincesj');
Route::get('districtsj/{province_id}', 'DistrictController@getDistrictsj');
Route::get('countriesj', 'CountryController@getCountriesj');
Route::get('countryj/{id}', 'CountryController@getCountry');
Route::get('provincej/{id}', 'ProvinceController@getProvince');
Route::get('districtj/{id}', 'DistrictController@getDistrict');



Route::resource('users', 'UserController');
Route::resource('permissions', 'PermissionController');
Route::resource('roles', 'RolController');
Route::resource('companies', 'CompanyController');
Route::resource('positions', 'PositionController');
Route::resource('categories', 'CategoryController');
Route::resource('items', 'ItemController');
Route::resource('type_clients', 'TypeClientController');
Route::resource('countries', 'CountryController');
Route::resource('provinces', 'ProvinceController');
Route::resource('districts', 'DistrictController');
