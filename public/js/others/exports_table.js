$(document).ready(function () {
    $.ajaxSetup({
        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
    });

    $(function () {
        $('body').on('click', '#frmExport #btn_filter', function (e) {
            e.preventDefault();
            data = $('#frmExport').serialize();
            console.log(data);
            $.ajax({
                type: 'POST',
                url: '/exports',
                data: data
            }).done(function (data) {
                $('.exports').html(data);
            }).fail(function () {
                alert('Articles could not be loaded.');
            });
        });
    });

    $(function () {
        $('body').on('click', '#loadexport .loadexport .pagination a', function (e) {
            e.preventDefault();
            $('#loadexport a').css('color', '#dfecf6');
            var url = $(this).attr('href');
            getBuys(url);
            window.history.pushState("", "", url);
        });

        function getBuys(url) {
            $.ajax({
                type: 'GET',
                url: url
            }).done(function (data) {
                $('.exports').html(data);
            }).fail(function () {
                alert('Articles could not be loaded.');
            });
        }
    });

    $(function () {
        $("#frmExport .select_all").click(function () {
            var checked = $(this).is(':checked');
            if (checked) {
                $("#frmExport  .periodo").attr('disabled',true);  // checked

                $("#frmExport  #tipo_cliente").attr('disabled',true);  // checked
                $("#frmExport #sexo").attr('disabled',true);
                $("#frmExport #nacionalidad").attr('disabled',true);
                $("#frmExport .provincia").attr('disabled',true);
                $("#frmExport #ciu").attr('disabled',true);

            }else{
                $("#frmExport  .periodo").attr('disabled',false);  // checked

                $("#frmExport  #tipo_cliente").attr('disabled',false);  // checked
                $("#frmExport #sexo").attr('disabled',false);
                $("#frmExport #nacionalidad").attr('disabled',false);
                $("#frmExport .provincia").attr('disabled',false);
                $("#frmExport #ciu").attr('disabled',false);
            }

        });


    });
});