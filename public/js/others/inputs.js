jQuery(document).ready(function ($) {
    $("#frmClient .rubros").select2({placeholder: "--Seleccionar rubro--", tags: true});
    $("#frmClient .empresa").select2();
    $("#frmClient .categoria").select2();
    $("#frmClient .cargo").select2();
    $("#frmClient .provincia").select2();
    $("#frmClient .localidad").select2();
    $("#frmClient .barrio").select2();
    $("#frmClient .country").select2();

    // $("#district_id").select2();

    $('input[name="fecha_inicio"]').daterangepicker({
        "singleDatePicker": true,
        "showCustomRangeLabel": false,
        locale: {

            format: 'DD/MM/YYYY',
            "daysOfWeek": [
                "Dom",
                "Lun",
                "Mar",
                "Mie",
                "Jue",
                "Vie",
                "Sab"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
        }
    });
    $('input[name="fecha_fin"]').daterangepicker({
        "singleDatePicker": true,
        "showCustomRangeLabel": false,
        locale: {

            format: 'DD/MM/YYYY',
            "daysOfWeek": [
                "Dom",
                "Lun",
                "Mar",
                "Mie",
                "Jue",
                "Vie",
                "Sab"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
        }
    });

    $('input[name="periodo"]').daterangepicker({
        "showDropdowns": true,
        "autoUpdateInput": false,
        "autoApply": true,

        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "weekLabel": "W",
            "daysOfWeek": [
                "Dom",
                "Lun",
                "Mar",
                "Mie",
                "Jue",
                "Vie",
                "Sab"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],

            "firstDay": 1
        },
        "opens": "left"

    });
    $('input[name="periodo"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });

    $('input[name="periodo"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $(function () {


        $('input[id="birthdate"]').daterangepicker({
            "singleDatePicker": true,
            "autoUpdateInput": true,
            "showDropdowns": true,
            format: 'DD/MM/YYYY',
            locale: {
                format: 'DD/MM/YYYY',
                "daysOfWeek": [
                    "Dom",
                    "Lun",
                    "Mar",
                    "Mie",
                    "Jue",
                    "Vie",
                    "Sab"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
            }
        });
        // function(start, end, label) {
        //     var years = moment().diff(start, 'years');
        //     $('input[id="edad"]').val(years);
        // });
    });
});
