/**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var loadCompaniesTable  = function () {
        var table = $('#companies').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            language: {
                url: "/js/datatables/datatable_spanish.json"
            },
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "ajax": "/companies/list",
            "columns": [
                {data: 'nombre'},
                {data: 'cuil'},

                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true
        });


        $("#companies tbody").on("click", "a.edit", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/companies/"+data.id+"/edit" ;
        });

        $("#companies tbody").on("click", "a.show", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/companies/"+data.id ;
        });

        $("#companies tbody").on("click", "a.delete", function () {
            var data = table.row($(this).parents("tr")).data();
            $("#frmCompanyDelete #id").val(data.id);
        });

    }
    loadCompaniesTable();

    $('#frmCompanyDelete #btn_eliminar').click(function (e) {
        e.preventDefault();
        $.ajax({
            method: 'delete',
            url: '/companies/'+$("#frmCompanyDelete #id").val(),
            success: function (response) {
                $('#modalDelete').modal('toggle');
                $('.modal-backdrop').remove();
                loadCompaniesTable();
            },
            error: function (data) {
                console.log(data);
            },
        }).done( function (data) {
                toastr.options.timeOut = 5000;
                toastr.warning('Se ha eliminado la empresa');
            }
        );
    });


});/**
 * Created by diego on 17/5/17.
 */
