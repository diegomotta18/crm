/**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#tipo_cliente').change(function(){
            var table = $('#clients').DataTable({
                dom : 'lfrtip',
                buttons: [
                    {
                        extend: 'excel',
                        text: '<span class="fa fa-file-excel-o"></span>Exportar',
                        exportOptions: {
                            modifier: {
                                search: 'applied',
                                order: 'applied'
                            }
                        }
                    }
                ],
                responsive: true,
                "destroy": true,
                "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
                "bLengthChange": true, //thought this line could hide the LengthMenu
                "bInfo": true,
                autoWidth: true,
                language: {
                    url: "/js/datatables/datatable_spanish.json"
                },
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                "ajax": "/clients/type/"+$('#tipo_cliente').val(),
                "columns": [
                    {data: 'email'},
                    {data: 'num_doc'},
                    {data: 'nombre'},
                    {data: 'apellido'},
                    {data: 'fecha_de_nacimiento'},

                    { "render": function (data, type, JsonResultRow, meta) {

                        if(JsonResultRow.address){
                            return JsonResultRow.address.district.name;
                        }else{
                            return "";
                        }
                    }},
                    {data: 'created_at'},

                    {data: 'action', name: 'action', orderable: false, searchable: false}

                ],
                "scroller": false,
                "scrollY": "600px",
                "scrollCollapse": true
            });

    });


    var loadClientesTable  = function () {
        var table = $('#clients').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            language: {
                url: "/js/datatables/datatable_spanish.json"
            },

            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "ajax": "/clients",
            "columns": [
                {data: 'email'},
                {data: 'num_doc'},
                {data: 'nombre'},
                {data: 'apellido'},
                {data: 'fecha_de_nacimiento'},

                { "render": function (data, type, JsonResultRow, meta) {

                    if(JsonResultRow.address){
                        if(JsonResultRow.address.district != null){
                            return JsonResultRow.address.district.name;
                        }else{
                            return "";
                        }
                    }else{
                        return "";
                    }
                }},
                {data: 'created_at'},

                {data: 'action', name: 'action', orderable: false, searchable: false}

            ],
            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true

        });



        $("#clients tbody").on("click", "a.edit", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/clients/"+data.id+"/edit" ;

        });

        $("#clients tbody").on("click", "a.show", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/clients/"+data.id ;

        });

        $("#clients tbody").on("click", "a.delete", function () {
            var data = table.row($(this).parents("tr")).data();
            $("#frmClienteDelete #id").val(data.id);
            $("#frmClienteDelete #email").val(data.email);

        });
    //
    }
    loadClientesTable();

    $('#frmClienteDelete #btn_eliminar').click(function (e) {
        e.preventDefault();
        $.ajax({
            method: 'delete',
            url: '/clients/'+$("#frmClienteDelete #id").val(),
            success: function (response) {
                $('#modalDelete').modal('toggle');
                $('.modal-backdrop').remove();
                loadClientesTable();
            },
            error: function (data) {
                console.log(data);
            },
        }).done( function (data) {
                toastr.options.timeOut = 5000;
                toastr.warning('Se ha eliminado el cliente '+$("#frmClienteDelete #email").text());
            }
        );
    });


});/**
 * Created by diego on 17/5/17.
 */
