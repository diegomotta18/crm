/**
 * Created by diego on 18/4/17.
 */
jQuery(document).ready(function($){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    var loadUserTable  = function () {
        var table = $('#usuarios').DataTable({
            responsive: true,
            "destroy": true,
            "lengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
            "bLengthChange": true, //thought this line could hide the LengthMenu
            "bInfo": true,
            autoWidth: true,
            language: {
                url: "/js/datatables/datatable_spanish.json"
            },
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "ajax": "/usersdt",
            "columns": [
                {data: 'name'},
                {data: 'email'},
                {data: 'action', name: 'action', orderable: false, searchable: false}

            ],
            "scroller": false,
            "scrollY": "600px",
            "scrollCollapse": true
        });



        $("#usuarios tbody").on("click", "a.edit", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/users/"+data.id+"/edit" ;

        });

        $("#usuarios tbody").on("click", "a.show", function () {
            var data = table.row($(this).parents("tr")).data();
            window.location.href = "/users/"+data.id ;

        });


        $("#usuarios tbody").on("click", "a.delete", function () {
            var data = table.row($(this).parents("tr")).data();
            $("#frmUserDelete #id").val(data.id);
            $("#frmUserDelete #email").val(data.email);
            $("#frmUserDelete #nombre").val(data.name);

        });

    }
    loadUserTable();



    $('#frmUserDelete #btn_eliminar').click(function (e) {
        e.preventDefault();
        $.ajax({
            method: 'delete',
            url: '/users/'+ $("#frmUserDelete #id").val(),
            success: function (response) {
                $('#modalDelete').hide();
                $('.modal-backdrop').remove();
            },
            error: function (data) {
                console.log(data);
            },
        }).done( function (data) {
                toastr.options.timeOut = 5000;
                toastr.warning('Se ha eliminado el usuario '+$("#frmUserDelete #email").text());
                loadUserTable();
            }
        );
    });


});/**
 * Created by diego on 17/5/17.
 */
