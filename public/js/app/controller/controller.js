app.controller('PositionCont', ['$scope', 'ContactoFact', function ($scope, ContactoFact) {

    $scope.positions = [];
    $scope.getPositions = function () {

        ContactoFact.getPositions().then(function (data) {
            $scope.positions = data;
        });

    };
    $scope.position = {id: ''};

    $scope.changeId = function (id) {
        $scope.position.id = id;
    };

    $scope.init = function () {
        $scope.getPositions();
    };

    $scope.init();

}]);

app.controller('CompanyCont', ['$scope', 'ContactoFact', function ($scope, ContactoFact) {
    $scope.companies = [];
    $scope.company = {id: ''};

    $scope.changeId = function (id) {
        $scope.company.id = id;
    }
    $scope.getCompanies = function () {

        ContactoFact.getCompanies().then(function (data) {
            $scope.companies = data;
        });

    }



    $scope.init = function () {
        $scope.getCompanies();
    }

    $scope.init();

}]);

app.controller('CategoryCont', ['$scope', 'ContactoFact', '$filter', function ($scope, ContactoFact) {

    $scope.categories = [];
    $scope.getCategories = function () {

        ContactoFact.getCategories().then(function (data) {
            $scope.categories = data;
        });

    }
    $scope.companies = [];
    $scope.category = {id: ''};

    $scope.changeId = function (id) {
        $scope.category.id = id;
    }

    $scope.init = function () {
        $scope.getCategories();
    }

    $scope.init();

}]);

app.controller('ItemCont', ['$scope', 'ContactoFact', '$filter', function ($scope, ContactoFact) {

    $scope.items = [];
    $scope.getItems = function () {

        ContactoFact.getItems().then(function (data) {
            $scope.items = data;
        });

    }


    $scope.init = function () {
        $scope.getItems();
    }

    $scope.init();

}]);

app.controller('ClientCont', ['$scope', 'ContactoFact', 'ClientServ', function ($scope, ContactoFact, ClientServ) {


    var ctrl = this;
    this.callServer = function callServer(tableState) {
        $scope.displayedClients = [];
        ctrl.tableStateUse = tableState;
        ctrl.isLoading = true;
        ctrl.tableState = tableState;
        var pagination = tableState.pagination;
        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 10;  // Number of entries showed per page.
        ClientServ.getPage(start, number, tableState).then(function (result) {
            $scope.filteredCollection = result.all;
            $scope.inicia = start;
            $scope.termina = result.items + start;
            $scope.displayedClients = (result.data); // st-table
            tableState.pagination.numberOfPages = result.numberOfPages;//set the number of pages so the pagination can update
            ctrl.isLoading = false;
        });

    };
    $scope.remove = function (id, row) {
        ContactoFact.deleteClient(id).then(function (data) {
            toastr.options.timeOut = 5000;
            toastr.warning('Se ha eliminado el cliente ');
            var index = $scope.displayedClients.indexOf(row);
            if (index !== -1) {
                // $scope.displayedClients.splice(index, 1);
                window.location.href = '/clients';

            }
        });

    }

    // $scope.showFilters = function () {
    //     console.log($scope.filteredCollection);
    //
    // }
}]);

app.controller('TypeClientCont', ['$scope', 'ContactoFact', '$timeout',function ($scope, ContactoFact, $timeout) {

    $scope.typeClients = [];
    $scope.getTypeClients = function () {
        ContactoFact.getTypeClients().then(function (data) {
            $scope.typeClients = data;
        });
    }


    $scope.init = function () {
        $scope.getTypeClients();
    }

    $scope.init();

}]);


app.controller('AddressPLCCont', ['$scope', 'ContactoFact', '$filter', function ($scope, ContactoFact) {
    $scope.countries = [];
    $scope.states = [];
    $scope.districts = [];
    $scope.towns = [];
    $scope.country = {id: ''};
    $scope.state = {id: ''};
    $scope.district = {id: ''};
    $scope.town = {id: ''};


    $scope.changeCountry = function (country_id,state_id, district_id, town_id) {
        $scope.country.id = country_id;
        $scope.getStates(country_id);
        $scope.state.id = state_id;
        $scope.getDistricts(state_id);
        $scope.district.id = district_id;
        $scope.getTowns(district_id);
        $scope.town.id = town_id;
    }
    $scope.getCountries = function () {

        ContactoFact.getCountries().then(function (data) {
            $scope.countries = data;
        });
    }

    $scope.getStates = function (code) {
        ContactoFact.getProvinces(code).then(function (data) {
            $scope.states = data;
        });
    }


    $scope.getDistricts = function (code) {

        ContactoFact.getCities(code).then(function (data) {
            $scope.districts = data;
            console.log($scope.districts);
            console.log($scope.district);
        });
    }

    $scope.getTowns = function (id) {
        ContactoFact.getPlaces(id).then(function (data) {
            $scope.towns = data;
        });
    }

    $scope.init = function () {
        $scope.getCountries();
    }

    $scope.init();

}]);



