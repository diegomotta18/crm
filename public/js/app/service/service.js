/**
 * Created by diego on 26/5/17.
 */
app.factory('ContactoFact', ['$http', 'api', '$filter', '$timeout', function ($http, api, $filter, $timeout) {
    var dataFactory = {};

    dataFactory.getPositions = function (id) {
        return ($http({
                url: api + 'positions/all',
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };

    dataFactory.getCompanies = function (id) {
        return ($http({
                url: api + 'companies/all',
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };

    dataFactory.getCategories = function (id) {
        return ($http({
                url: api + 'categories/all',
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };

    dataFactory.getItems = function (id) {
        return ($http({
                url: api + 'items/all',
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };

    dataFactory.getClients = function (id) {
        return ($http({
                url: api + 'clients/all',
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };

    dataFactory.deleteClient = function (id) {
        return ($http({
                url: api + 'clients/' + id,
                method: "DELETE",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };


    dataFactory.deleteTypeClient = function (id) {
        return ($http({
                url: api + 'type_clients/' + id,
                method: "DELETE",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };

    dataFactory.getTypeClients = function () {
        return ($http({
                url: api + 'type_clients/all',
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };

    dataFactory.getClients = function (id) {
        return ($http({
                url: api + 'clients/all',
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };
    dataFactory.getCountries = function () {
        return ($http({
                url: api + 'countries/',
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );
    };
    dataFactory.getProvinces = function (code) {
        return ($http({
                url: api + 'provinces/' + code,
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };

    dataFactory.getCities = function (code) {
        return ($http({
                url: api + 'districs/' + code,
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };


    dataFactory.getPlaces = function (id_district) {

        return ($http({
                url: api + 'places/' + id_district,
                method: "GET",

            }).then(function (data, status, headers, config) {
                return data.data;

            }, function (err) {
                error = err;
            })
        );

    };

    return dataFactory;
}]);