<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientCreateRequest;
use App\Http\Requests\ClientUpdateRequest;
use App\Repositories\ClientRepository;
use App\Models\TypeClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;


class ClientController extends Controller
{
    //
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function index()
    {
        //
        if (auth()->user()->can('Ver clientes')) {
            return view('clients.index');
        }
        return abort(404);
    }


    public function storedclient(Request $request)
    {
        return $this->clientRepository->storedclient($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        if (auth()->user()->can('Crear cliente')) {
            if (ctype_digit($request->input('tipo_de_cliente')) && !is_null($request->input('tipo_de_cliente'))) {
                return $this->verifyTypeCliente($request);
            } else {
                $notification = array(
                    'message' => "Debe seleccionar el tipo de cliente",
                    'alert-type' => 'error'
                );
                return Redirect::to(route('clients.index'))->with($notification);
            }
        }
        return abort(404);

    }

    /* Verifica si el tipo de cliente seleccionado no es nulo*/

    public function verifyTypeCliente(Request $request)
    {
        $typeclient = TypeClient::find($request->input('tipo_de_cliente'));
        if (!is_null($typeclient)) {
            return view('clients/create', compact('typeclient'));
        }
    }


    public function edit($id)
    {
        if (auth()->user()->can('Modificar cliente')) {
            return $this->clientRepository->edited($id);
        }
        return abort(404);
    }


    public function store(ClientCreateRequest $request)
    {
        //
        if (auth()->user()->can('Crear cliente')) {
            $client = $this->clientRepository->created($request);
            $notification = array(
                'message' => "Se ha creado el cliente " . $client->nombre . " " . $client->apellido,
                'alert-type' => 'success'
            );
            return Redirect::to(route('clients.index'))->with($notification);
        }
        return abort(404);
    }

    public function update(ClientUpdateRequest $request, $id)
    {
        if (auth()->user()->can('Modificar cliente')) {
            $this->clientRepository->updated($request, $id);
            $notification = array(
                'message' => "Se ha modificado el cliente " . $request->input('nombre') . " " . $request->input('apellido'),
                'alert-type' => 'success'
            );
            return Redirect::to(route('clients.index'))->with($notification);
        }
        return abort(404);
    }

    public function show($id)
    {
        if (ctype_digit($id) && !is_null($id)) {
            $client = $this->clientRepository->findForId($id);
            return $this->clientRepository->showed($client);

        }

    }

    public function destroy($id)
    {
        if (auth()->user()->can('Eliminar cliente')) {
            $client = $this->clientRepository->find($id);
            $this->clientRepository->delete($client);
            return [];

        }
        return abort(404);
    }

    /* Valida que exista una seleccion de un tipo de cliente para crear nuevos clientes*/


    public function changeForm(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'tipo_de_cliente' => 'required',
        ]);
        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());

        } else {
            $typeclient = TypeClient::find($request->input('tipo_de_cliente'));

            if ($typeclient->type_data == true) {
                return view('clients.create', compact('typeclient'));
            } else {
                return view('clients.createWithoutCompany', compact('typeclient'));
            }
        }
    }

    /* Crear un  cliente con los datos de la empresa*/

    public function storeWithoutCompany(ClientCreateRequest $request)
    {
        if (auth()->user()->can('Crear cliente')) {

            $this->clientRepository->storeWithoutCompany($request);

            $notification = array(
                'message' => "Se ha creado el cliente " . $request->input('nombre') . " " . $request->input('apellido'),
                'alert-type' => 'success'
            );
            return Redirect::to(route('clients.index'))->with($notification);
        }
        return abort(404);

    }


    /* Actualiza los datos de cliente con los datos de la empresa*/

    public function updateWithoutCompany(ClientUpdateRequest $request, $id)
    {
        if (auth()->user()->can('Modificar cliente')) {

            if (ctype_digit($id) && !is_null($id)) {

                $this->clientRepository->updateWithoutCompany($request, $id);

                $notification = array(
                    'message' => "Se ha modificado el cliente " . $request->input('nombre') . " " . $request->input('apellido'),
                    'alert-type' => 'success'
                );
                return Redirect::to(route('clients.index'))->with($notification);

            } else {
                return Redirect::to(route('clients.index'));
            }
        }
        return abort(404);
    }


    public function getAllClients()
    {
        return $this->clientRepository->getAllClients();

    }


    public function getClientsForType($id)
    {
        return $this->clientRepository->getClientsForType($id);

    }

    public function importFile()
    {
        $validation = Validator::make(request()->all(), [
            'archivo' => 'required'
        ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation->errors());
        } else {
            if ($this->clientRepository->importFile() == 404) {
                $notification = array(
                    'message' => "Formato de archivo de importación incorrecto",
                    'alert-type' => 'danger'
                );
            } else {
                $notification = array(
                    'message' => "Se ha importado el archivo correctamente",
                    'alert-type' => 'success'
                );
            };
            return Redirect::to(route('clients.index'))->with($notification);
        }
    }


}