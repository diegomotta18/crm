<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUser;
use App\Http\Requests\UpdateUser;
use App\Models\User;
use App\Repositories\UserRepository;

use Illuminate\Support\Facades\Redirect;

use DataTables;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    protected $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth');
        $this->userRepository = $userRepository;
    }

    public function index(){
        $this->authorize('view',auth()->user());
        return view('users.index');

    }

    public function create(){
        $this->authorize('create',auth()->user());
        $roles = Role::all();
        return view('users.create',compact('roles'));
    }

    public function  edit($id){
        $this->authorize('update',auth()->user());

        $user = $this->userRepository->find($id);
        $roles = Role::all();

        return view('users.edit',compact('user','roles'));
    }

    public  function  btnsUsuarios(){
        return ' 
        <div style="display: flex;">
             <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                <ul class="dropdown-menu" style="position: initial;">
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalShow"class="btn btn-xs show"><i style="color: #337ab7;" class="glyphicon glyphicon-eye-open"></i> Ver</a></li>
                    
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalEdit" class="btn btn-xs  edit"><i style="color: #1ab394;" class="glyphicon glyphicon-edit"></i> Editar</a></li>
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs delete"><i style="color: #f90d0d;" class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
        
                </ul>

             </div>
        </div>';
    }

    public function getUsers(){
        return DataTables::eloquent(User::query())
            ->addColumn('action',function(){
                return $this->btnsUsuarios();
            })->make(true);
    }

    protected function store(CreateUser $request)
    {
        $this->authorize('create',auth()->user());

        $user = $this->userRepository->created($request);
        $notification = array(
            'message' => "Se ha creado el usuario ".  $user->name,
            'alert-type' => 'success'
        );
        return Redirect::to(route('users.index'))->with($notification);

    }

    public function update(UpdateUser $request,$id){
        $this->authorize('update',auth()->user());

        $user = $this->userRepository->updated($request,$id);
        $notification = array(
            'message' => "Se ha modificado el usuario ".  $user->name,
            'alert-type' => 'success'
        );
        return Redirect::to(route('users.index'))->with($notification);

    }

    public function  show($id){
        $user = $this->userRepository->find($id);
        return view('users.show', compact('user'));
    }

    public function destroy($id){
        $this->authorize('delete',auth()->user());
        $user = $this->userRepository->find($id);
        $this->userRepository->delete($user);
        return [];

    }
}
