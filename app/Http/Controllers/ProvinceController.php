<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProvinceRequest;
use App\Models\Country;
use App\Models\Province;
use DataTables;
use Illuminate\Support\Facades\Redirect;

class ProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        if (auth()->user()->can('Ver provincias')) {
            return view('address.provinces.index');
        }
        return abort(404);

    }

    public function getProvincesj($id){
        $provinces = Province::where('country_id',$id)->get();
        return $provinces->toJson();
    }

    public function getProvince($id){
        $provinces = Province::findOrFail($id);
        return $provinces->toJson();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->can('Crear provincia')) {
            return view('address.provinces.create');
        }
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProvinceRequest $request)
    {
        if (auth()->user()->can('Crear provincia')) {

            $country = Country::findOrFail($request->input('country_id'));
            $province = Province::create([
                'name' => $request->input('nombre')
            ]);
            $country->provinces()->save($province);
            $notification = array(
                'message' => "Se ha creado la provincia " . $province->name,
                'alert-type' => 'success'
            );
            return Redirect::to(route('provinces.index'))->with($notification);
        }
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function show($id)
//    {
//        //
//        return null
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->can('Modificar provincia')) {
            $province = Province::findOrFail($id);
            return view('address.provinces.edit', compact('province'));
        }
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProvinceRequest $request, $id)
    {
        if (auth()->user()->can('Modificar provincia')) {
            $province = Province::findOrFaild($id);
            $province->name = $request->input('nombre');
            $province->save();
            $notification = array(
                'message' => "Se ha modifica la provincia " . $province->name,
                'alert-type' => 'success'
            );
            return Redirect::to(route('provinces.index'))->with($notification);
        }
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->user()->can('Eliminar provincia')) {
            $province = Province::findOrFail($id);
            $province->delete();
            return [];
        }
        return abort(404);
    }


    public  function  btnsProvinces(){
        return ' 
        <div style="display: flex;">
             <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                <ul class="dropdown-menu" style="position: initial;">                    
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalEdit" class="btn btn-xs  edit"><i style="color: #1ab394;" class="glyphicon glyphicon-edit"></i> Editar</a></li>
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs delete"><i style="color: #f90d0d;" class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
                </ul>
             </div>
        </div>';
    }

    public function getProvinces($id){
        return DataTables::eloquent(Province::where('country_id',$id))
            ->addColumn('action',function(){
                return $this->btnsProvinces();
            })->make(true);
    }
}
