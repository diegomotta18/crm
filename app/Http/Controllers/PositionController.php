<?php

namespace App\Http\Controllers;

use App\Http\Requests\PositionCreateRequest;
use App\Http\Requests\PositionUpdateRequest;
use App\Models\Position;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Redirect;
use DataTables;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        if (auth()->user()->can('Ver cargos')) {

            return view('parameters.positions.index');
        }
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (auth()->user()->can('Crear cargo')) {

            return view('parameters.positions.create');
        }
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PositionCreateRequest $request)
    {
        //
        if (auth()->user()->can('Crear cargo')) {

            Position::create([
                'nombre' => $request->input('nombre'),
            ]);
            $notification = array(
                'message' => "Se ha creado el cargo " . $request->input('nombre'),
                'alert-type' => 'success'
            );
            return Redirect::to(route('positions.index'))->with($notification);
        }
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('parameters.positions.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (auth()->user()->can('Modificar cargo')) {

            $this->authorize('update', auth()->user());

            $position = Position::find($id);
            return view('parameters.positions.edit', compact('position'));
        }
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PositionUpdateRequest $request, $id)
    {
        //
        if (auth()->user()->can('Modificar cargo')) {

            $position = Position::find($id);
            $position->nombre = $request->input('nombre');
            $position->save();
            $notification = array(
                'message' => "Se ha modificado el cargo " . $request->input('nombre'),
                'alert-type' => 'success'
            );
            return Redirect::to(route('positions.index'))->with($notification);
        }
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (auth()->user()->can('Eliminar cargo')) {

            $position = Position::find($id);
            $position->delete();
            return [];

        }
        return abort(404);
    }

    public function getPositions()
    {
        return Datatables::eloquent(Position::query())
            ->addColumn('action', function () {
                return $this->btnsPosition();
            })->make(true);
    }

    public function btnsPosition()
    {
        return '<div class="btn-group" role="group" aria-label="...">
            <a href="#"   class="btn btn-xs btn-warning edit"><i class="glyphicon glyphicon-edit"></i> </a>
            <a href="#" data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs btn-danger delete"><i class="glyphicon glyphicon-remove"></i> </a></div>
             ';
    }

    public function getAllPosition()
    {
        $position = Position::all();
        return new JsonResponse($position);
    }
}
