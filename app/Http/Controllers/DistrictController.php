<?php

namespace App\Http\Controllers;

use App\Http\Requests\DistrictRequest;
use App\Models\District;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DataTables;

class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (auth()->user()->can('Ver ciudades')) {

            return view('address.districts.index');
        }
        return abort(404);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (auth()->user()->can('Crear ciudad')) {

            return view('address.districts.create');
        }
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(DistrictRequest $request)
    {
        //
        if (auth()->user()->can('Crear ciudad')) {

            $province = Province::findOrFail($request->input('province_id'));
            $district = District::create([
                'name' => $request->input('nombre')
            ]);
            $province->districts()->save($district);
            $notification = array(
                'message' => "Se ha creado la ciudad " . $district->name,
                'alert-type' => 'success'
            );
            return Redirect::to(route('districts.index'))->with($notification);
        }
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        return Redirect::to(route('districts.index'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (auth()->user()->can('Modificar ciudad')) {

            $district = District::findOrFail($id);
            return view('address.districts.edit', compact('district'));
        }
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(DistrictRequest $request, $id)
    {
        //
        if (auth()->user()->can('Modificar ciudad')) {

            $this->authorize('update', auth()->user());

            $district = District::findOrFail($id);
            $district->name = $request->input('nombre');
            $district->save();
            $notification = array(
                'message' => "Se ha modificado la ciudad " . $district->name,
                'alert-type' => 'success'
            );
            return Redirect::to(route('districts.index'))->with($notification);
        }
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (auth()->user()->can('Eliminar ciudad')) {

            $this->authorize('delete', auth()->user());

            $district = District::findOrFail($id);
            $district->delete();
            return [];
        }
        return abort(404);

    }

    public function btnsDistricts()
    {
        return ' 
        <div style="display: flex;">
             <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                <ul class="dropdown-menu" style="position: initial;">     
                               
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalEdit" class="btn btn-xs  edit"><i style="color: #1ab394;" class="glyphicon glyphicon-edit"></i> Editar</a></li>
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs delete"><i style="color: #f90d0d;" class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
        
                </ul>

             </div>
        </div>';
    }

    public function getDistricts($id)
    {
        return DataTables::eloquent(District::where('province_id', $id))
            ->addColumn('action', function () {
                return $this->btnsDistricts();
            })->make(true);
    }

    public function getDistrictsj($id)
    {
        $provinces = District::where('province_id', $id)->get();
        return $provinces->toJson();
    }

    public function getDistrict($id)
    {
        $district = District::findOrFail($id);
        return $district->toJson();
    }
}
