<?php

namespace App\Http\Controllers;


use App\Http\Requests\CompanyCreateRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Models\Company;
use App\Repositories\CompanyRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Redirect;
use DataTables;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $companyRepository;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->middleware('auth');
        $this->companyRepository = $companyRepository;
    }

    public function index()
    {
        //
        if (auth()->user()->can('Ver empresas')) {

            return view('parameters.companies.index');
        }
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->can('Crear empresa')) {
            return view('parameters.companies.create');
        }
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyCreateRequest $request)
    {
        if (auth()->user()->can('Crear empresa')) {

            $this->authorize('create', auth()->user());

            $company = $this->companyRepository->created($request);
            $notification = array(
                'message' => "Se ha creado la empresa " . $company->nombre,
                'alert-type' => 'success'
            );
            return Redirect::to(route('companies.index'))->with($notification);
        }
        return abort(404);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('parameters.companies.index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (auth()->user()->can('Modificar empresa')) {
            $company = $this->companyRepository->find($id);
            return view('parameters.companies.edit', compact('company'));
        }
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyUpdateRequest $request, $id)
    {
        //
        if (auth()->user()->can('Modificar empresa')) {
            $company = $this->companyRepository->updated($request, $id);
            $notification = array(
                'message' => "Se ha modificado la empresa " . $company->nombre,
                'alert-type' => 'success'
            );
            return Redirect::to(route('companies.index'))->with($notification);
        }
        return abort(404);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (auth()->user()->can('Eliminar empresa')) {

            $company = $this->companyRepository->find($id);
            $this->companyRepository->delete($company);
            return [];
        }
        return bort(404);
    }

    public function getCompanies()
    {
        return Datatables::eloquent(Company::query())
            ->addColumn('action', function () {
                return $this->btnsCompany();
            })->make(true);
    }

    public function btnsCompany()
    {
        return '<div class="btn-group" role="group" aria-label="...">
            <a href="#"   class="btn btn-xs btn-warning edit"><i class="glyphicon glyphicon-edit"></i> </a>
            <a href="#" data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs btn-danger delete"><i class="glyphicon glyphicon-remove"></i> </a></div>';
    }

    public function getAllCompany()
    {
        //
        $company = Company::all();
        return new JsonResponse($company);
    }
}