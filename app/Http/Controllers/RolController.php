<?php

namespace App\Http\Controllers;


use App\Http\Requests\RolUpdateRequest;
use App\Repositories\RolRepository;
use App\Http\Requests\RolRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use DataTables;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolController extends Controller
{
    protected $rolRepository;
    public function __construct(RolRepository $rolRepository)
    {
        $this->middleware('auth');
        $this->rolRepository = $rolRepository;
    }

    public function index(){
        $this->authorize('view',auth()->user());
        return view('roles.index');

    }

    public function create(){
        $this->authorize('create',auth()->user());
        return view('roles.create');
    }

    public function  edit($id){
        $this->authorize('update',auth()->user());
        $rol = $this->rolRepository->find($id);
        return view('roles.edit',compact('rol'));
    }

    public  function  btnsRoles(){
        return ' 
        <div style="display: flex;">
             <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                <ul class="dropdown-menu" style="position: initial;">
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalAssign"class="btn btn-xs assign"><i style="color:#f8ac59;" class="fa fa-check-square"></i> Asignar permisos</a></li>
                    
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalEdit" class="btn btn-xs  edit"><i style="color: #1ab394;" class="glyphicon glyphicon-edit"></i> Editar</a></li>
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs delete"><i style="color: #f90d0d;" class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
        
                </ul>

             </div>
        </div>';
    }

    public function getRoles(){
        return DataTables::eloquent(Role::query())
            ->addColumn('action',function(){
                return $this->btnsRoles();
            })->make(true);
    }

    protected function store(RolRequest $request)
    {
        $this->authorize('create',auth()->user());

        $rol = $this->rolRepository->created($request);
        $notification = array(
            'message' => "Se ha creado el rol ".  $rol->name,
            'alert-type' => 'success'
        );
        return Redirect::to(route('roles.index'))->with($notification);

    }

    public function update(RolUpdateRequest $request,$id){
        $this->authorize('update',auth()->user());

        $rol = $this->rolRepository->updated($request,$id);
        $notification = array(
            'message' => "Se ha modificado el rol ".  $rol->name,
            'alert-type' => 'success'
        );
        return Redirect::to(route('roles.index'))->with($notification);

    }

    public function  show($id){
        $rol = $this->rolRepository->find($id);
        return view('roles.show', compact('rol'));
    }

    public function destroy($id){
        $this->authorize('delete',auth()->user());
        $rol = $this->rolRepository->find($id);
        $this->rolRepository->delete($rol);
        return [];

    }

    public function getPermissions($id){
        $permisos = Permission::orderBy('created_at', 'desc')->get();
        $rol = $this->rolRepository->find($id);
        return view('permissions.index',compact('permisos','rol'));
    }

    public function updatePermissions(Request $request,$id){
        if (auth()->user()->can('Asignar permisos')) {
            $rol = $this->rolRepository->updatePermissions($request,$id);
            $notification = array(
                'message' => "Se ha asignado permisos al rol ".  $rol->name,
                'alert-type' => 'success'
            );
            return Redirect::to(route('roles.index'))->with($notification);
        }else{
            return Redirect::to(route('home'));

        }


    }
}
