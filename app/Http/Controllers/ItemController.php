<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemCreateRequest;
use App\Http\Requests\ItemUpdateRequest;
use App\Models\Item;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Redirect;
use DataTables;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        if (auth()->user()->can('Ver rubros')) {

            return view('parameters.items.index');
        }
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->can('Crear rubro')) {

            return view('parameters.items.create');
        }
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemCreateRequest $request)
    {
        //
        if (auth()->user()->can('Crear rubro')) {

            Item::create([
                'nombre' => $request->input('nombre'),
            ]);
            $notification = array(
                'message' => "Se ha creado el rubro " . $request->input('nombre'),
                'alert-type' => 'success'
            );
            return Redirect::to(route('items.index'))->with($notification);
        }
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('parameters.items.index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (auth()->user()->can('Modificar rubro')) {

            $item = Item::find($id);
            return view('parameters.items.edit', compact('item'));
        }
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ItemUpdateRequest $request, $id)
    {
        //

        if (auth()->user()->can('Modificar rubro')) {

            $item = Item::find($id);
            $item->nombre = $request->input('nombre');
            $item->save();
            $notification = array(
                'message' => "Se ha modificado el rubro " . $request->input('nombre'),
                'alert-type' => 'success'
            );
            return Redirect::to(route('items.index'))->with($notification);
        }
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (auth()->user()->can('Eliminar rubro')) {

            $item = Item::find($id);
            $item->delete();
            return [];
        }
        return abort(404);
    }

    public function getItems()
    {
        return Datatables::eloquent(Item::query())
            ->addColumn('action', function () {
                return $this->btnsItems();
            })->make(true);
    }

    public function btnsItems()
    {

        return '<div class="btn-group" role="group" aria-label="...">
                <a href="#"  data-toggle="modal" data-target="#modalEdit"  class="btn btn-xs btn-warning edit"><i class="glyphicon glyphicon-edit"></i> </a>
                <a href="#" data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs btn-danger delete"><i class="glyphicon glyphicon-remove"></i> </a></div>';
    }

    public function getAllItem()
    {
        //
        $item = Item::all();
        return new JsonResponse($item);
    }
}
