<?php

namespace App\Http\Controllers;


use App\Http\Requests\PermissionUpdateRequest;
use App\Repositories\PermissionRepository;
use App\Http\Requests\PermissionRequest;
use Illuminate\Support\Facades\Redirect;

use DataTables;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    protected $permissionRepository;
    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->middleware('auth');
        $this->permissionRepository = $permissionRepository;
    }

    public function index(){
        $this->authorize('view',auth()->user());
        return view('permissions.index_root');

    }

    public function create(){
        $this->authorize('create',auth()->user());
        return view('permissions.create');
    }

    public function  edit($id){
        $this->authorize('update',auth()->user());
        $permission = $this->permissionRepository->find($id);
        return view('permissions.edit',compact('permission'));
    }

    public  function  btnsPermissions(){
        return ' 
        <div style="display: flex;">
             <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                <ul class="dropdown-menu" style="position: initial;">                    
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalEdit" class="btn btn-xs  edit"><i style="color: #1ab394;" class="glyphicon glyphicon-edit"></i> Editar</a></li>
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs delete"><i style="color: #f90d0d;" class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
        
                </ul>

             </div>
        </div>';
    }

    public function getPermissions(){
        return DataTables::eloquent(Permission::query())
            ->addColumn('action',function(){
                return $this->btnsPermissions();
            })->make(true);
    }

    protected function store(PermissionRequest $request)
    {
        $this->authorize('create',auth()->user());

        $permission = $this->permissionRepository->created($request);
        $notification = array(
            'message' => "Se ha creado el permiso ".  $permission->name,
            'alert-type' => 'success'
        );
        return Redirect::to(route('permissions.index'))->with($notification);

    }

    public function update(PermissionUpdateRequest $request,$id){
        $this->authorize('update',auth()->user());

        $permission = $this->permissionRepository->updated($request,$id);
        $notification = array(
            'message' => "Se ha modificado el permiso ".  $permission->name,
            'alert-type' => 'success'
        );
        return Redirect::to(route('permissions.index'))->with($notification);

    }

    public function destroy($id){
        $this->authorize('delete',auth()->user());
        $rol = $this->permissionRepository->find($id);
        $this->permissionRepository->delete($rol);
        return [];

    }


}
