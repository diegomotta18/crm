<?php

namespace App\Http\Controllers;

use App\Http\Requests\CountryRequest;
use App\Models\Country;
use Illuminate\Support\Facades\Redirect;
use DataTables;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        if (auth()->user()->can('Ver paises')) {
            return view('address.countries.index');
        }
        return abort(404);

    }

    public function getCountry($id)
    {
        $country = Country::findOrFail($id);
        return $country->toJson();

    }

    public function getCountriesj()
    {
        $countries = Country::all();

        return $countries->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (auth()->user()->can('Crear país')) {
            return view('address.countries.create');
        }
        return abort(404);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CountryRequest $request)
    {
        //
        if (auth()->user()->can('Crear país')) {
            $country = Country::create([
                'name' => $request->input('nombre')
            ]);
            $notification = array(
                'message' => "Se ha creado el país " . $country->name,
                'alert-type' => 'success'
            );
            return Redirect::to(route('countries.index'))->with($notification);
        }
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('address.countries.index');


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (auth()->user()->can('Modificar país')) {

            $country = Country::findOrFail($id);
            return view('address.countries.edit', compact('country'));
        }
        return abort(404);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CountryRequest $request, $id)
    {
        //
        if (auth()->user()->can('Modificar país')) {

            $country = Country::findOrFail($id);
            $country->name = $request->input('nombre');
            $country->save();
            $notification = array(
                'message' => "Se ha modificado el paías " . $country->name,
                'alert-type' => 'success'
            );
            return Redirect::to(route('countries.index'))->with($notification);
        }
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        if (auth()->user()->can('Eliminar país')) {

            $country = Country::findOrFail($id);
            $country->delete();
            return [];
        }
        return abort(404);
    }

    public function btnsCountries()
    {
        return ' 
        <div style="display: flex;">
             <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                <ul class="dropdown-menu" style="position: initial;">                    
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalEdit" class="btn btn-xs  edit"><i style="color: #1ab394;" class="glyphicon glyphicon-edit"></i> Editar</a></li>
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs delete"><i style="color: #f90d0d;" class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
        
                </ul>

             </div>
        </div>';
    }

    public function getCountries()
    {
        return DataTables::eloquent(Country::query())
            ->addColumn('action', function () {
                return $this->btnsCountries();
            })->make(true);
    }
}
