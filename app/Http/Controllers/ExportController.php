<?php

namespace App\Http\Controllers;

use App\Events\ExportWasReceived;
use App\Models\Client;
use App\Models\District;
use App\Models\Province;
use App\Models\TypeClient;
use App\Repositories\ClientRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use Illuminate\Support\Collection as Collection;

class ExportController extends Controller
{
    //
    protected $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
//        $this->middleware('auth');
        $this->clientRepository = $clientRepository;
    }


    public function index(Request $request)
    {
        $clients = Client::paginate(10);
        $filters = null;
        if ($request->ajax()) {
            return view('renders.exports_table', ['clients' => $clients, 'filters' => $filters ])->render();
        }
        return view('export.index', compact('clients','filters'));
    }

    /*
     *  En el request vienen los filtros a aplicar para crear el listado y el xls.
     *  Ademas para mostrar la info de los filtros aplicados se utiliza $filters = new Collection
     * */
    public function filter(Request $request)
    {
        $filters = new Collection;
        $pushed = new Collection;
        $clients = null;
        if ($request->has('select_all')){
            $clients = Client::all();
            Excel::create('Clientes', function ($excel) use ($clients) {
                $excel->sheet('Clientes', function ($sheet) use ($clients) {
                    $sheet->fromArray($clients, null, 'A1', true);
                });
            })->store('xls', storage_path('exports'));
            $clients = Client::paginate(10);
        }else {
            $clients = (new Client)->newQuery()->with(['address:id,province_id,district_id'])->with(['address.province:id,name'])->with(['address.district:id,name']);
            if (!is_null($request->input('periodo'))) {
                $filters->push($request->input('periodo'));
                $fechas = explode(" - ", $request->input('periodo'));
                $fecha_in = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[0])));
                $fecha_out = date('Y-m-d', strtotime(str_replace('/', '-', $fechas[1])));
                $clients->whereBetween('created_at', array($fecha_in, $fecha_out));
            }
            if (!is_null($request->input('nacionalidad'))) {
                if (Input::get('nacionalidad') == false) {
                    $filters->push("Argentino");
                } else {
                    $filters->push("Extranjero");
                }
                $clients->where('extranjero', '=', Input::get('nacionalidad'));
            }
            if (!is_null($request->input('tipo_de_cliente'))) {
                $filters->push(TypeClient::find(Input::get('tipo_de_cliente'))->first()->nombre);
                $clients->where('type_client_id', '=', Input::get('tipo_de_cliente'));
            }
            if (!is_null($request->input('sexo'))) {
                $filters->push($request->input('sexo'));
                $clients->where('sexo', '=', Input::get('sexo'));
            }
            if (!is_null($request->input('provincia'))) {
                $filters->push((Province::find(Input::get('provincia')))->name);
                if (!is_null($request->input('ciudad'))) {
                    $filters->push((District::find(Input::get('ciudad')))->name);
                    $clients->whereHas('address', function ($query) {
                        $query->where('province_id', '=', Input::get('provincia'))->where('district_id', '=', Input::get('ciudad'));
                    });
                } else {
                    $clients->whereHas('address', function ($q) {
                        $q->where('province_id', '=', Input::get('provincia'));
                    });

                }
            }
            $clients->chunk(100, function ($users) use ($pushed) {
                foreach ($users as $user) {
                    $pushed->push(['email' => $user->email]);
                }
            });
            Excel::create('Clientes', function ($excel) use ($pushed) {
                $excel->sheet('Clientes', function ($sheet) use ($pushed) {
                    $sheet->fromArray($pushed, null, 'A1', true);
                });
            })->store('xls', storage_path('exports'));
            $clients = $clients->paginate(10);

        }

        if ($request->ajax()) {
            return view('renders.exports_table', ['clients' => $clients, 'filters' => $filters])->render();
        }
        return view('export.index', compact('clients', 'filters'));
    }


    public function export()
    {
        return response()->download(storage_path("/exports/Clientes.xls"));

    }


}