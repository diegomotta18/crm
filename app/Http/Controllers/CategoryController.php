<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoriesCreateRequest;
use App\Http\Requests\CategoriesUpdateRequest;
use App\Models\Category;
use App\Repositories\BaseRepository;
use App\Repositories\CategoryRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Redirect;
use DataTables;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $categoryRep;
    public function __construct(CategoryRepository $categoryRep)
    {
        $this->middleware('auth');
        $this->categoryRep = $categoryRep;
    }

    public function index()
    {
        //
        $this->authorize('view',auth()->user());
        return view('parameters.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create',auth()->user());
        return view('parameters.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesCreateRequest $request)
    {
        //
        $this->authorize('create',auth()->user());
        $category = $this->categoryRep->created($request);
        $notification = array(
            'message' => "Se ha creado la categoría " . $category->nombre,
            'alert-type' => 'success'
        );
        return Redirect::to(route('categories.index'))->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('parameters.categories.index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->authorize('update',auth()->user());

        $category = $this->categoryRep->find($id);
        return view('parameters.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriesUpdateRequest $request, $id)
    {
        //
        $this->authorize('update',auth()->user());

        $category  =$this->categoryRep->updated($request,$id);
        $notification = array(
            'message' => "Se ha modificado la categoría " . $category->nombre,
            'alert-type' => 'success'
        );
        return Redirect::to(route('categories.index'))->with($notification);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $this->authorize('delete',auth()->user());

        $category = $this->categoryRep->find($id);
        $this->categoryRep->delete($category);
        return [];
    }

    public function getCategory()
    {
        return Datatables::eloquent(Category::query())
            ->addColumn('action', function () {
                return $this->btnsCategories();
            })->make(true);
    }

    public function btnsCategories()
    {
        if (auth()->user()->role == 'god' or auth()->user()->role == 'admin') {
            return '<div class="btn-group" role="group" aria-label="...">
                <a href="#"   class="btn btn-xs btn-warning edit"><i class="glyphicon glyphicon-edit"></i> </a>
                <a href="#" data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs btn-danger delete"><i class="glyphicon glyphicon-remove"></i> </a></div>';
        } else {
            return '<div class="btn-group" role="group" aria-label="...">
                <a href="#"   class="btn btn-xs btn-warning edit"><i class="glyphicon glyphicon-edit"></i> </a>';
        }
    }

    public function getAllCategory()
    {
        //
        $category = Category::all();
        return new JsonResponse($category);
    }
}
