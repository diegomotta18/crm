<?php

namespace App\Http\Controllers;

use App\Http\Requests\TypeClientCreateRequest;
use App\Http\Requests\TypeClientUpdateRequest;
use App\Models\TypeClient;
use App\Repositories\TypeClientRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Redirect;
use DataTables;

class TypeClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $type;

    public function __construct(TypeClientRepository $type)
    {
        $this->middleware('auth');
        $this->type = $type;
    }

    public function index()
    {
        //
        if (auth()->user()->can('Ver tipos de clientes')) {

            return view('parameters.type_clients.index');
        }
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (auth()->user()->can('Crear tipo de cliente')) {
            return view('parameters.type_clients.create');
        }
        return abort(404);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TypeClientCreateRequest $request)
    {
        if (auth()->user()->can('Crear tipo de cliente')) {

            $type = $this->type->created($request);
            $notification = array(
                'message' => "Se ha creado el tipo de cliente " . $type->nombre,
                'alert-type' => 'success'
            );
            return Redirect::to(route('type_clients.index'))->with($notification);
        }
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('parameters.type_clients.index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (auth()->user()->can('Modificar tipo de cliente')) {

            $type_client = $this->type->find($id);
            return view('parameters.type_clients.edit', compact('type_client'));
        }
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(TypeClientUpdateRequest $request, $id)
    {
        //
        if (auth()->user()->can('Modificar tipo de cliente')) {

            $type = $this->type->updated($request, $id);
            $notification = array(
                'message' => "Se ha modificado el tipo de cliente " . $type->nombre,
                'alert-type' => 'success'
            );
            return Redirect::to(route('type_clients.index'))->with($notification);
        }
        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (auth()->user()->can('Eliminar tipo de cliente')) {

            $type = $this->type->find($id);
            $this->type->delete($type);
            return null;
        }
        return abort(404);
    }

    public function getTypeClientAll()
    {
        $typeClients = TypeClient::all();
        return new JsonResponse($typeClients);
    }

    public function btnsTypeClient()
    {
        return ' 
        <div style="display: flex;">
             <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                <ul class="dropdown-menu" style="position: initial;">
                    
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalEdit" class="btn btn-xs edit"><i style="color: #1ab394;" class="glyphicon glyphicon-edit"></i> Editar</a></li>
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs delete"><i style="color: #f90d0d;" class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
        
                </ul>

             </div>
        </div>';
    }

    public function getTypes()
    {
        return Datatables::eloquent(TypeClient::query())
            ->addColumn('action', function () {
                return $this->btnsTypeClient();
            })->make(true);
    }

}
