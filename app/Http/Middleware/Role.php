<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;

class Role
{
    protected $hierarchy = [
        'god' => 1,
        'adm' => 2,
        'user' => 3,
    ];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {

        $user = auth()->user();
        if($user) {
            if ($this->hierarchy[$user->role] < $this->hierarchy[$role]) {
                Redirect::to(route('home'));
                //return $next($request);
            }


            return $next($request);

        }
        else{
            Redirect::to(url('/'));
        }
    }
}