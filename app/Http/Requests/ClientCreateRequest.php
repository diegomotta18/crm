<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nombre' => 'required|max:100',
            'apellido' => 'required|max:50',
            'n°_documento' => 'required|max:9|unique:clients,num_doc,NULL,id,deleted_at,NULL',
            'fecha_de_nacimiento' => 'required',
            'sexo' => 'required',
            'email' => 'required',
            'telefono' => 'required',
            'celular' => 'required',
            'tipo_de_cliente','pais','domicilio','provincia','localidad','ciudad',
            'empresa','categoria','rubro','cargo','interes'
        ];
    }
}
