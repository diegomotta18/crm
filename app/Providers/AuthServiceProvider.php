<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Client;
use App\Models\Company;
use App\Models\Country;
use App\Models\District;
use App\Models\Item;
use App\Models\Position;
use App\Models\Province;
use App\Models\TypeClient;
use App\Models\User;
use App\Policies\CategoryPolicy;
use App\Policies\ClientPolicy;
use App\Policies\CompanyPolicy;
use App\Policies\CountryPolicy;
use App\Policies\DistrictPolicy;
use App\Policies\ItemPolicy;
use App\Policies\PermissionPolicy;
use App\Policies\PositionPolicy;
use App\Policies\ProvincePolicy;
use App\Policies\RolPolicy;
use App\Policies\TypeClientPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => PermissionPolicy::class,
        User::class => UserPolicy::class,
        User::class => RolPolicy::class,
//        Country::class => CountryPolicy::class,
//        Province::class => ProvincePolicy::class,
//        District::class => DistrictPolicy::class,
//        Client::class => ClientPolicy::class,
//        Category::class => CategoryPolicy::class,
//        Position::class => PositionPolicy::class,
//        TypeClient::class => TypeClientPolicy::class,
//        Item::class => ItemPolicy::class,
//        Company::class => CompanyPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();


    }
}
