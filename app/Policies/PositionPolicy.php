<?php

namespace App\Policies;

use App\Models\User;
use Spatie\Permission\Models\Role;

use Illuminate\Auth\Access\HandlesAuthorization;

class PositionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the role.
     *
     * @param  \App\Models\User  $user
     * @param  \Spatie\Permission\Models\Role  $role
     * @return mixed
     */
    public function view(User $user)
    {
        //
        $valor = false;
        foreach($user->getRoleNames() as $role){
            $rol = Role::findByName($role);
            if ($rol->hasPermissionTo('Ver cargos')){
                $valor = true;
            }
        }
        return $valor;
    }

    /**
     * Determine whether the user can create roles.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        $valor = false;
        foreach($user->getRoleNames() as $role){
            $rol = Role::findByName($role);
            if ($rol->hasPermissionTo('Crear cargo')){
                $valor = true;
            }
        }
        return $valor;
    }

    /**
     * Determine whether the user can update the role.
     *
     * @param  \App\Models\User  $user
     * @param  \Spatie\Permission\Models\Role  $role
     * @return mixed
     */
    public function update(User $user)
    {
        //
        $valor = false;
        foreach($user->getRoleNames() as $role){
            $rol = Role::findByName($role);
            if ($rol->hasPermissionTo('Modificar cargo')){
                $valor = true;
            }
        }
        return $valor;
    }

    /**
     * Determine whether the user can delete the role.
     *
     * @param  \App\Models\User  $user
     * @param  \Spatie\Permission\Models\Role  $role
     * @return mixed
     */
    public function delete(User $user)
    {
        //
        $valor = false;
        foreach($user->getRoleNames() as $role){
            $rol = Role::findByName($role);
            if ($rol->hasPermissionTo('Eliminar cargo')){
                $valor = true;
            }
        }
        return $valor;
    }


}
