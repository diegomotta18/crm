<?php

namespace App\Policies;

use App\Models\Country;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Spatie\Permission\Models\Role;

class ClientPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function view(User $user, Country $country)
    {
        //
        dd($country);
        $valor = false;
        foreach($user->getRoleNames() as $role){
            $rol = Role::findByName($role);
            if ($rol->hasPermissionTo('Ver clientes')){
                $valor = true;
            }
        }
        return $valor;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        $valor = false;
        foreach($user->getRoleNames() as $role){
            $rol = Role::findByName($role);
            if ($rol->hasPermissionTo('Crear cliente')){
                $valor = true;
            }
        }
        return $valor;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function update(User $user)
    {
        //
        $valor = false;
        foreach($user->getRoleNames() as $role){
            $rol = Role::findByName($role);
            if ($rol->hasPermissionTo('Modificar cliente')){
                $valor = true;
            }
        }
        return $valor;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function delete(User $user)
    {
        //
        $valor = false;
        foreach($user->getRoleNames() as $role){
            $rol = Role::findByName($role);
            if ($rol->hasPermissionTo('Eliminar cliente')){
                $valor = true;
            }
        }
        return $valor;
    }
}
