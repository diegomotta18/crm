<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TypeClient extends Entity
{
    //
    use SoftDeletes;

    protected $table = 'type_clients';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id','nombre','descripcion','type_data'
    ];

    public function client()
    {
        return $this->hasOne('App\Models\Client');
    }
}
