<?php

namespace App\Models;
use App\Models\Entity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Place extends Entity
{
    use SoftDeletes;

    //
    protected $table = 'places';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id','name','district_id'
    ];


    public function district(){
        return $this->belongsTo('App\Models\District');
    }


}
