<?php

namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Province extends Entity
{
    //
    use SoftDeletes;

    protected $table = 'provinces';

    protected $fillable = [
        'id','name','country_id'
    ];


    public function country(){
        return $this->belongsTo('\App\Models\Country');
    }

    public function districts(){
        return $this->hasMany('App\Models\District');
    }
}
