<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Entity
{
    //
    use SoftDeletes;

    protected $table = 'items';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id','nombre'
    ];
    public function company()
    {
        return $this->belongsToMany('App\Models\Company')
            ->withTimestamps();
    }

    public function category()
    {
        return $this->belongsToMany('App\Models\Category')
            ->withTimestamps();
    }
}
