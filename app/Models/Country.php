<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Entity
{
    //
    use SoftDeletes;

    protected $table = 'countries';

    protected $fillable = [
        'id','name'
    ];



    public function provinces(){
        return $this->hasMany('App\Models\Province');
    }
}
