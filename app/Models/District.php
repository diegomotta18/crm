<?php

namespace App\Models;
use App\Models\Entity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class District extends Entity
{
    //
    use SoftDeletes;

    protected $table = 'districts';

    protected $fillable = [
        'id','name','province_id'
    ];


    public function state(){
        return $this->belongsTo('App\Models\Province');
    }



}
