<?php

namespace App\Models;

use App\Models\Entity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends Entity
{
    //
    use SoftDeletes;

    protected $table = 'positions';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id','nombre'
    ];

    public function client()
    {
        return $this->hasOne('App\Models\Client');
    }
}
