<?php

namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Entity
{
    //
    use SoftDeletes;

    protected $table = 'categories';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id','nombre','company_id'
    ];
//    public function contacts(){
//     return $this->hasMany(Contact::class);
//    }

    public function company()
    {
        return $this->hasOne('App\Models\Company');
    }

    public function items()
    {
        return $this->hasMany('App\Models\Items');
    }
}
