<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Entity
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = ['id','country_id','province_id','district_id','place_id','address'];

    public function client()
    {
        return $this->hasOne(Client::class);
    }


    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

}
