<?php

namespace App\Models;

use App\Models\Entity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Entity
{
    //
    use SoftDeletes;

    protected $table = 'companies';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'id','nombre','cuil'
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function client()
    {
        return $this->hasOne('App\Models\Client');
    }

    public function items()
    {
        return $this->belongsToMany('App\Models\Item');
    }
}
