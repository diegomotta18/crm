<?php

namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Entity
{
    //
    use SoftDeletes;

    protected $table = 'clients';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id','extranjero','nombre','apellido','fecha_de_nacimiento','num_doc','sexo','telefono','celular','email','sitio_web','address_id','position_id','company_id','type_client_id','interes',
    ];
    public function typeclient()
    {
        return $this->belongsTo('App\Models\TypeClient');
    }
    public function position()
    {
        return $this->belongsTo('App\Models\Position');
    }

    public function service()
    {
        return $this->hasMany('App\Models\Services');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function address()
    {
        return $this->belongsTo('App\Models\Address');
    }

    public function getFechaDeNacimientoAttribute($value){
        return date('d/m/Y',strtotime(str_replace('-','/',$value)));
    }


    //scopes
    /**
     * Scope a query to only include users of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeExistEmail($query, $type)
    {
        return $query->where('email', $type);
    }

}
