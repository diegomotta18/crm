<?php

namespace App\Repositories;

use App\Http\Requests\RolRequest;
use App\Http\Requests\RolUpdateRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RolRepository extends BaseRepository
{
    /**
     * @param Model $items
     */
    public function __construct(Role $items)
    {
        $this->items = $items;
    }

    public function created(RolRequest $request){
        $attributes = ['name'=> $request->input('nombre')];
        $rol = $this->create($attributes);
        return $rol;
    }

    public function updated(RolUpdateRequest $request,$id){
        $rol = Role::find($id);
        $rol->name =  $request->input('nombre');
        $rol->save();
        return $rol;
    }

    public function updatePermissions(Request $request,$id){
        $rol = $this->find($id);
        $permisos = $request->input('permisos');
        $rol->permissions()->detach();
        foreach ($permisos as $permiso){
                $rol->givePermissionTo($permiso);
        }
        $rol->save();
        return $rol;
    }

}