<?php

namespace App\Repositories;

use App\Http\Requests\ClientCreateRequest;
use App\Http\Requests\ClientUpdateRequest;
use App\Models\Address;
use App\Models\Category;
use App\Models\Company;
use App\Models\Country;
use App\Models\District;
use App\Models\Item;
use App\Models\Place;
use App\Models\Position;
use App\Models\Province;
use App\Models\TypeClient;
use App\Models\Client;
use DataTables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;

class ClientRepository extends BaseRepository
{
    /**
     * @param Client $items
     */
    public function __construct(Client $items)
    {
        $this->items = $items;
    }

    /*
     * Visualiza el cliente
     * */
    public function showed($client)
    {
        $company = null;
        $position = null;
        $country = null;
        $state = null;
        $district = null;
        $place = null;
        $rubros = null;
        $typeclient = TypeClient::find($client->type_client_id);
        if (!is_null($client)) {
            if (is_null($typeclient)) {
                $company = Company::with('items')->find($client->company_id);
                if (!is_null($company)) {
                    $category = Category::find($company->category_id);
                    $position = Position::find($client->position_id);
                }
                if ($client->address_id) {
                    $address = Address::find($client->address_id);
                    $country = Country::where('id', '=', $address->country_id)->select('name')->first();
                    if ($address->province_id) {
                        $provincia = Province::where('id', '=', $address->province_id)->select('name')->first();
                    }
                    if ($address->district_id) {
                        $district = District::find($address->district_id);
                    }
                    if ($address->place_id) {
                        $place = Place::find($address->place_id);
                    }
                }
                $rubros = Item::all();
                if (!is_null($rubros)) {
                    $rubrosIds = [];
                    $rubrosCompany = $company->items()->get();
                    foreach ($rubrosCompany as $rubrosC) {
                        $rubrosIds[] = $rubrosC->id;
                    }
                }
                return view('clients/show', compact('typeclient', 'client', 'company', 'position', 'address', 'rubros', 'rubrosIds', 'category', 'country', 'provincia', 'district', 'place'));
            } else if ($typeclient->type_data) {
                $company = Company::with('items')->find($client->company_id);
                $category = Category::find($company->category_id);
                $position = Position::find($client->position_id);
                $country = null;
                $district = null;
                $provincia = null;
                $place = null;
                if ($client->address_id) {
                    $address = Address::find($client->address_id);
                    $country = Country::where('id', '=', $address->country_id)->select('name')->first();
                    if ($address->province_id) {
                        $provincia = Province::where('id', '=', $address->province_id)->select('name')->first();
                    }
                    if ($address->district_id) {
                        $district = District::find($address->district_id);
                    }
                    if ($address->place_id) {
                        $place = Place::find($address->place_id);
                    }
                }
                $rubros = Item::all();
                $rubrosIds = [];
                $rubrosCompany = $company->items()->get();
                foreach ($rubrosCompany as $rubrosC) {
                    $rubrosIds[] = $rubrosC->id;
                }
                return view('clients/show', compact('typeclient', 'client', 'company', 'position', 'address', 'rubros', 'rubrosIds', 'category', 'country', 'provincia', 'district', 'place'));
            } else {
                $typeclient = null;
                if ($client->address_id > 0) {
                    $address = Address::find($client->address_id);
                    $country = Country::find($address->country_id);
                    if ($address->province_id > 0 ) {
                        $provincia = Province::find($address->province_id);
                    }
                    if ($address->district_id) {
                        $district = District::find($address->district_id);
                    }
                    if ($address->place_id) {
                        $place = Place::find($address->place_id);
                    }
                }
                return view('clients/show', compact('typeclient', 'client', 'address', 'country', 'provincia', 'district', 'place'));
            }

        }
        return view('clients/index');
    }

    /*
     * Actualiza el registro cliente
     *
     * */

    public function edited($id)
    {
        $company = null;
        $position = null;
        $country = null;
        $state = null;
        $district = null;
        $place = null;
        $rubros = null;
        $address = null;
        $client = $this->findForId($id);
        if (!is_null($client)) {
            $typeclient = TypeClient::find($client->type_client_id);
            if (is_null($typeclient)) {
                $company = Company::with('items')->find($client->company_id);
                if (!is_null($company)) {
                    $category = Category::find($company->category_id);
                    $position = Position::find($client->position_id);
                    $rubros = Item::all();
                    $rubrosIds = [];
                    $rubrosCompany = $company->items()->get();
                    foreach ($rubrosCompany as $rubrosC) {
                        $rubrosIds[] = $rubrosC->id;
                    }
                }

                if (!is_null($client->address_id)) {
                    $address = Address::find($client->address_id)->first();
                }

                return view('clients/edit', compact('address', 'client', 'company', 'position', 'address', 'rubros', 'rubrosIds', 'category', 'typeclient'));
            }
            if ($typeclient->type_data == true) {
                $company = Company::with('items')->find($client->company_id);
                $position = Position::find($client->position_id);
                $address = null;
                if ($client->address_id) {
                    $address = Address::find($client->address_id);
                }
                $rubros = Item::all();
                $rubrosIds = [];
                $rubrosCompany = $company->items()->get();
                foreach ($rubrosCompany as $rubrosC) {
                    $rubrosIds[] = $rubrosC->id;
                }
                if (!is_null($address)) {

                    return view('clients/edit', compact('client', 'company', 'position', 'address', 'rubros', 'rubrosIds', 'typeclient', 'place'));
                }
                return view('clients/edit', compact('client', 'company', 'position', 'address', 'rubros', 'rubrosIds', 'typeclient'));
            } else {
                $address = null;
                if ($client->address_id) {
                    $address = Address::findOrFail($client->address_id);
                }
                return view('clients.editWithoutCompany', compact('client', 'address', 'typeclient'));
            }
        } else {
            return Redirect::to(route('home'));
        }
    }


    public function updated(ClientUpdateRequest $request, $id)
    {
        $address = null;
        $client = Client::with('company')->find($id);
        if (!is_null($client)) {
            $attributes = [
                'nombre' => $request->input('nombre'),
                'apellido' => $request->input('apellido'),
                'email' => $request->input('email'),
                'sitio_web' => $request->input('sitio_web'),
                'sexo' => $request->input('sexo'),
                'num_doc' => $request->input('n°_documento'),
                'fecha_de_nacimiento' => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('fecha_de_nacimiento')))),
                'telefono' => $request->input('telefono'),
                'celular' => $request->input('celular'),
                'interes' => $request->input('interes'),
            ];

            if ($client->address_id) {
                $address = Address::find($client->address_id);
                $address->country_id = $request->input('pais');
                $address->province_id = $request->input('provincia');
                $address->district_id = $request->input('ciudad');
                $address->place_id = $request->input('localidad');
                $address->address = $request->input('domicilio');
                $address->save();
            } elseif ($request->input('pais') or $request->input('domicilio') or $request->input('provincia') or $request->input('ciudad') or $request->input('localidad')) {
                $address = Address::create([
                    'country_id' => $request->input('pais'),
                    'province_id' => $request->input('provincia'),
                    'district_id' => $request->input('ciudad'),
                    'place_id' => $request->input('localidad'),
                    'address' => $request->input('domicilio'),
                ]);
                $attributes = array_merge($attributes, ['address_id' => $address->id]);

            }

            $companyClient = Company::with('category')->find($client->company_id);
            if (!is_null($companyClient)) {
                $company = Company::find($request->input('empresa'));
                $categoryCompany = Category::with('company')->find($companyClient->category_id);
                $category = Category::with('company')->find($request->input('categoria'));
                $position = Position::find($request->input('cargo'));
                $positionClient = Position::find($client->position_id);
                if ($company->id != $companyClient->id) {
                    $companyClient->category()->dissociate();
                    $companyClient->save();

                    if ($categoryCompany->id == $category->id) {
                        $company->category()->associate($categoryCompany);
                    } else {
                        $company->category()->associate($category);
                        $categoryCompany = $category;
                    }
                    $company->save();
                    $attributes = array_merge($attributes, ['company_id' => $company->id]);
                    $companyClient = $company;
                }

                if ($categoryCompany->id != $category->id) {
                    $companyClient->category()->dissociate();
                    $companyClient->category()->associate($category);
                    $companyClient->save();
                }

                if ($position->id != $positionClient->id) {
                    $attributes = array_merge($attributes, ['position_id' => $position->id]);

                }

                $rubros = $request->input('rubros');
                $companyClient->items()->detach();
                foreach ($rubros as $rubro) {
                    $companyClient->items()->attach($rubro);
                    $companyClient->save();
                }
            }
            $item = $this->update( $id,$attributes);
            return $item;
        }
    }

    /*
     * Crea el registro cliente
     *
     * */
    public function created(ClientCreateRequest $request)
    {
        $type_client = TypeClient::find($request->input('tipo_de_cliente'));

        $attributes = [
            'nombre' => $request->input('nombre'),
            'apellido' => $request->input('apellido'),
            'email' => $request->input('email'),
            'sitio_web' => $request->input('sitio_web'),
            'sexo' => $request->input('sexo'),
            'num_doc' => $request->input('n°_documento'),
            'fecha_de_nacimiento' => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('fecha_de_nacimiento')))),
            'telefono' => $request->input('telefono'),
            'celular' => $request->input('celular'),
            'interes' => $request->input('interes'),
            'type_client_id' => $type_client->id
        ];

        if ($request->input('pais') or $request->input('domicilio') or $request->input('provincia') or $request->input('ciudad') or $request->input('localidad')) {
            $address = Address::create([
                'country_id' => $request->input('pais'),
                'province_id' => $request->input('provincia'),
                'district_id' => $request->input('ciudad'),
                'place_id' => $request->input('localidad'),
                'address' => $request->input('domicilio'),
            ]);

            $attributes = array_merge($attributes, ['address_id' => $address->id]);
        }
        if ($type_client->id != 1 && $type_client->id != 2) {
            $company = Company::find($request->input('empresa'));
            $category = Category::find($request->input('categoria'));
            $category->company()->save($company);
            $rubros = $request->input('rubros');
            foreach ($rubros as $rubro) {
                $item = Item::find($rubro);
                $company->items()->attach($item);
            }
            $position = Position::find($request->input('cargo'));
            $attributes = array_merge($attributes, ['position_id' => $position->id]);
            $attributes = array_merge($attributes, ['company_id' => $company->id]);

        }

        $item = $this->create($attributes);
        return $item;
    }

    //realizar la busqueda del cliente mediante el id
    public function findForId($id)
    {
        $client = Client::with('address:id,country_id,province_id,district_id')->with('address.province:id,name')->with(['address.district:id,name'])->find($id);
        return $client;
    }

    //Obtiene todos los clientes por orden de creacion
    public function getAllClients()
    {

        $clients = Client::with(['address.district:id,name'])->where('status', true)->orderBy('created_at', 'DESC');
        return DataTables::of($clients)->addColumn('action', function () {
            return $this->btnsClients();
        })->make(true);
    }

    //Obtiene los clientes por tipo de cliente, se manda el id del tipo de cliente
    public function getClientsForType($id)
    {
        $clients = Client::with(['address.district:id,name'])->where('type_client_id', '=', $id)->where('status', true)->orderBy('created_at', 'DESC')->get();
        return DataTables::of($clients)->addColumn('action', function () {
            return $this->btnsClients();
        })->make(true);;

    }

    public function btnsClients()
    {
        return '<div style="display: flex;">
             <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                <ul class="dropdown-menu" style="position: initial;">

                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalShow"class="btn btn-xs show"><i style="color: #337ab7;" class="glyphicon glyphicon-eye-open"></i> Ver</a></li>
                    
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalEdit" class="btn btn-xs  edit"><i style="color: #1ab394;" class="glyphicon glyphicon-edit"></i> Editar</a></li>
                    <li style="border-bottom: 1px solid #efefef;"><a data-toggle="modal" data-target="#modalDelete"  class="btn btn-xs delete"><i style="color: #f90d0d;" class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
        
                </ul>

             </div>
        </div>';
    }

    /*
     * Importa los datos de un excel, xls o csv
     *
     * Se debe respetar los encabezados del excel por ahora se pide "email","first_name","last_name","identification","telephone","birthday","type_client"
     * */
    public function importFile()
    {
        $path = Input::file('archivo')->getRealPath();
        Excel::load($path, function ($results) {
            $results = $results->first();
            if (!empty($results) && $results->count()) {
                //Indica que solo tiene la cabecera de email
                if ($results[0]->count() > 1) {
                    $results = $this->unique_multidim_array($results, 'email');
                    $results = $this->unique_multidim_array($results, 'identification');
                    foreach ($results as $key => $value) {
                        $extranjero = null;
                        if ($value->extranjero == 'No') {
                            $extranjero = 'false';
                        } else {
                            $extranjero = 'true';
                        }
                        $client = Client::where('email', $value->email)->first();
                        if (is_null($client)) {
                            $address = null;
                            if (!is_null($value->country_id)) {
                                if ($value->district_id){

                                        $address = Address::create([
                                            'country_id' => $value->country_id,
                                            'province_id' => $value->state_id,
                                            'district_id' => $value->district_id,
                                        ]);


                                }else{
                                    $address = Address::create([
                                        'country_id' => $value->country_id,
                                        'province_id' => $value->state_id,
                                        'district_id' => null,
                                    ]);
                                }

                                if (!is_null($address)){
                                    Client::create([
                                        'email' => $value->email,
                                        'nombre' => $value->first_name,
                                        'apellido' => $value->last_name,
                                        'num_doc' => $value->identification,
                                        'telefono' => $value->telephone,
                                        'fecha_de_nacimiento' => date('Y-m-d', strtotime(str_replace('/', '-', $value->birthday))),
                                        'extranjero' => $extranjero,
                                        'type_client_id' => 2,
                                        'address_id' => $address->id
                                    ]);
                                }else{
                                    Client::create([
                                        'email' => $value->email,
                                        'nombre' => $value->first_name,
                                        'apellido' => $value->last_name,
                                        'num_doc' => $value->identification,
                                        'telefono' => $value->telephone,
                                        'fecha_de_nacimiento' => date('Y-m-d', strtotime(str_replace('/', '-', $value->birthday))),
                                        'extranjero' => $extranjero,
                                        'type_client_id' => 2,
                                        'address_id' => null,
                                    ]);
                                }

                            } else {
                                Client::create([
                                    'email' => $value->email,
                                    'nombre' => $value->first_name,
                                    'apellido' => $value->last_name,
                                    'num_doc' => $value->identification,
                                    'telefono' => $value->telephone,
                                    'fecha_de_nacimiento' => date('Y-m-d', strtotime(str_replace('/', '-', $value->birthday))),
                                    'extranjero' => $extranjero,
                                    'type_client_id' => 2,
                                ]);
                            }
                        }
                    }
                }
            }
        });
    }


    //almacena los datos del cliente sin los datos de la compañia
    public function storeWithoutCompany(ClientCreateRequest $request)
    {
        $type_client = TypeClient::find($request->input('tipo_de_cliente'));
        $attributes = [
            'nombre' => $request->input('nombre'),
            'apellido' => $request->input('apellido'),
            'email' => $request->input('email'),
            'sitio_web' => $request->input('sitio_web'),
            'sexo' => $request->input('sexo'),
            'num_doc' => $request->input('n°_documento'),
            'fecha_de_nacimiento' => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('fecha_de_nacimiento')))),
            'telefono' => $request->input('telefono'),
            'celular' => $request->input('celular'),
            'type_client_id' => $type_client->id,
            'interes' => $request->input('interes'),
        ];

        if ($request->input('pais') or $request->input('domicilio') or $request->input('provincia') or $request->input('ciudad') or $request->input('localidad')) {
            $address = Address::create([
                'country_id' => $request->input('pais'),
                'province_id' => $request->input('provincia'),
                'district_id' => $request->input('ciudad'),
                'place_id' => $request->input('localidad'),
                'address' => $request->input('domicilio'),
            ]);
            $attributes = array_merge($attributes, ['address_id' => $address->id]);
        }
        return $this->create($attributes);
    }

    //Actualizar los datos del cliente sin los datos de la compañia
    public function updateWithoutCompany(ClientUpdateRequest $request, $id)
    {
        $client = $this->find($id);
        if (!is_null($client)) {
            $attributes = [
                'nombre' => $request->input('nombre'),
                'apellido' => $request->input('apellido'),
                'email' => $request->input('email'),
                'sitio_web' => $request->input('sitio_web'),
                'sexo' => $request->input('sexo'),
                'num_doc' => $request->input('n°_documento'),
                'fecha_de_nacimiento' => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('fecha_de_nacimiento')))),
                'telefono' => $request->input('telefono'),
                'celular' => $request->input('celular'),
                'interes' => $request->input('interes'),
            ];
            if ($client->address_id) {
                $address = Address::find($client->address_id);
                $address->country_id = $request->input('pais');
                $address->province_id = $request->input('provincia');
                $address->district_id = $request->input('ciudad');
                $address->place_id = $request->input('localidad');
                $address->address = $request->input('domicilio');
                $address->save();
            } elseif ($request->input('pais') or $request->input('domicilio') or $request->input('provincia') or $request->input('ciudad') or $request->input('localidad')) {
                $address = Address::create([
                    'country_id' => $request->input('pais'),
                    'province_id' => $request->input('provincia'),
                    'district_id' => $request->input('ciudad'),
                    'place_id' => $request->input('localidad'),
                    'address' => $request->input('domicilio'),
                ]);
                $attributes = array_merge($attributes, ['address_id' => $address->id]);
            }
            $item = $this->update($client, $attributes);
            return $item;
        }
    }

    /*
     * Endpoint para capturar los datos clientes desde cualquier sitio
     *
     * */
    public function storedclient($request)
    {
        //Elige como cliente potenciales los datos del cliente que viene por el endpoint
        $tipocliente = TypeClient::find(2);
        $attributes = [];
        if (!is_null($request->input('pais')) && !is_null($request->input('provincia')) && !is_null($request->input('localidad'))) {
            $address = new Address();
            $address->country_id = $request->input('pais');
            $address->province_id = $request->input('provincia');
            $address->district_id = $request->input('localidad');
            $address->save();
            $attributes = array_merge($attributes, ['address_id' => $address->id]);
        }
        if (!is_null($request->input('argentino'))) {
            $attributes = array_merge($attributes, ['extranjero' => false]);

        } elseif (!is_null($request->input('extranjero'))) {
            $attributes = array_merge($attributes, ['extranjero' => true]);
        }

        //nombre del input de documento de comunidad
        if (!is_null($request->input('documento'))) {
            $attributes = array_merge($attributes, ['num_doc' => $request->input('documento')]);

        }
        if (!is_null($request->input('sexo'))) {
            $attributes = array_merge($attributes, ['num_doc' => $request->input('documento')]);

        }
        $attributes = array_merge($attributes, [
            'nombre' => $request->input('nombres'),
            'apellido' => $request->input('apellido'),
            'email' => $request->input('email'),
            'fecha_de_nacimiento' => date('Y-m-d', strtotime(str_replace('/', '-', $request->input('fecha_de_nacimiento')))),
            'telefono' => $request->input('telefono'),
            'sexo' => $request->input('sexo'),
            'type_client_id' => $tipocliente->id
        ]);
        //return $attributes;
        return $this->create($attributes);
    }

    public function unique_multidim_array($array, $key)
    {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }
}