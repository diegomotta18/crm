<?php

namespace App\Repositories;

use App\Http\Requests\CategoriesCreateRequest;
use App\Http\Requests\CategoriesUpdateRequest;
use App\Http\Requests\TypeClientCreateRequest;
use App\Http\Requests\TypeClientUpdateRequest;
use App\Models\Category;

class CategoryRepository extends BaseRepository
{
    /**
     * @param Category $items
     */
    public function __construct(Category $items)
    {
        $this->items = $items;
    }


    public function created(CategoriesCreateRequest $request){

        $attributes = [
            'nombre' => $request->input('nombre'),
        ];
        $category = $this->create($attributes);
        return $category;
    }

    public function updated(CategoriesUpdateRequest $request,$id){

        $attributes= [ 'nombre' => $request->input('nombre')];
        $category = $this->update($id,$attributes);
        return $category;
    }
}