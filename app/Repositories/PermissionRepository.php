<?php

namespace App\Repositories;

use App\Http\Requests\PermissionRequest;
use App\Http\Requests\PermissionUpdateRequest;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class PermissionRepository extends BaseRepository
{
    /**
     * @param Model $items
     */
    public function __construct(Permission $items)
    {
        $this->items = $items;
    }

    public function created(PermissionRequest $request){
        $attributes = ['name'=> $request->input('nombre')];
        $permission = $this->create($attributes);
        return $permission;
    }

    public function updated(PermissionUpdateRequest $request,$id){
        $permission = Permission::find($id);
        $permission->name =  $request->input('nombre');
        $permission->save();
        return $permission;
    }



}