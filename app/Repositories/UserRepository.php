<?php

namespace App\Repositories;

use App\Http\Requests\CreateUser;
use App\Http\Requests\UpdateUser;
use App\Mail\SendMailUser;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UserRepository extends BaseRepository
{
    	/**
	 * @param Model $items
	 */
	public function __construct(User $items)
	{
		$this->items = $items;
	}

    public function created(CreateUser $request){
        $user = User::create([
            'name' => $request->input('nombre'),
            'email' => $request->input('email'),

            'password' => bcrypt($request->input('contraseña')),
        ]);
        $user->assignRole($request->input('rol'));
        Mail::to($user)->queue(new SendMailUser($user));
        return $user;
    }

    public function updated(UpdateUser $request,$id){
        $usuario = $this->find($id);
        $usuario->name = $request->input('nombre');
        $usuario->email = $request->input('email');
        $usuario->roles()->detach();
        $usuario->save();
        $usuario->assignRole($request->input('rol'));
        return $usuario;
    }
}