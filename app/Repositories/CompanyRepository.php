<?php

namespace App\Repositories;

use App\Http\Requests\CategoriesCreateRequest;
use App\Http\Requests\CategoriesUpdateRequest;
use App\Http\Requests\CompanyCreateRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Http\Requests\TypeClientCreateRequest;
use App\Http\Requests\TypeClientUpdateRequest;
use App\Models\Category;
use App\Models\Company;

class CompanyRepository extends BaseRepository
{
    /**
     * @param Company $items
     */
    public function __construct(Company $items)
    {
        $this->items = $items;
    }


    public function created(CompanyCreateRequest $request){

        $attributes = [
            'nombre' => $request->input('nombre'),
            'cuil' => $request->input('cuil')
        ];
        $company = $this->create($attributes);
        return $company;
    }

    public function updated(CompanyUpdateRequest $request,$id){

        $attributes= [
            'nombre' => $request->input('nombre'),
            'cuil' => $request->input('cuil')
        ];
        $company = $this->update($id,$attributes);
        return $company;
    }
}