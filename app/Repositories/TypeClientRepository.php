<?php

namespace App\Repositories;

use App\Http\Requests\TypeClientCreateRequest;
use App\Http\Requests\TypeClientUpdateRequest;
use App\Models\TypeClient;

class TypeClientRepository extends BaseRepository
{
    /**
     * @param TypeClient $items
     */
    public function __construct(TypeClient $items)
    {
        $this->items = $items;
    }


    public function created(TypeClientCreateRequest $request){
        $valor = null;
        if($request->has('type_data_empresa')){
            $valor = true;
        }
        else{
            $valor = false;
        }
        $attributes = [
            'nombre' => $request->input('nombre'),
            'descripcion' => $request->input('descripcion'),
            'type_data' => $valor,
        ];
        $type = $this->create($attributes);
        return $type;
    }

    public function updated(TypeClientUpdateRequest $request,$id){
        $valor = null;
        if($request->has('type_data_empresa')){
            $valor = true;
        }
        else{
            $valor = false;
        }
        $attributes = [
            'nombre' => $request->input('nombre'),
            'descripcion' => $request->input('descripcion'),
            'type_data' => $valor,
        ];
        $type = $this->update($id,$attributes);
        return $type;
    }
}