<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->insert([ 'nombre' => 'comercios minoristas']);
        DB::table('categories')->insert([ 'nombre' => 'comercios mayoristas']);
        DB::table('categories')->insert([ 'nombre' => 'bares - restaurantes - hoteles y afines']);
        DB::table('categories')->insert([ 'nombre' => 'reparaciones - talleres y otros']);
        DB::table('categories')->insert([ 'nombre' => 'industrias manufactureras y otras']);
        DB::table('categories')->insert([ 'nombre' => 'servicios tecnicos y profesionales']);
        DB::table('categories')->insert([ 'nombre' => 'servicios sociales y personales']);
        DB::table('categories')->insert([ 'nombre' => 'establecimientos financieros, de seguros y de bienes inmuebles']);
        DB::table('categories')->insert([ 'nombre' => 'agricultura, ganaderia y silvicultura']);
        DB::table('categories')->insert([ 'nombre' => 'transporte']);
        DB::table('categories')->insert([ 'nombre' => 'varios']);
    }
}
