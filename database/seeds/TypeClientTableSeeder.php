<?php

use Illuminate\Database\Seeder;

class TypeClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('type_clients')->insert(['nombre' => 'Potenciales integrantes de comunidad']);
        DB::table('type_clients')->insert(['nombre' => 'Potenciales consumidores']);
        DB::table('type_clients')->insert(['nombre' => 'Empresas', 'type_data'=> true]);
        DB::table('type_clients')->insert(['nombre' => 'Comercios', 'type_data'=> true]);
        DB::table('type_clients')->insert(['nombre' => 'Agencia de publicidad', 'type_data'=> true]);


    }
}
