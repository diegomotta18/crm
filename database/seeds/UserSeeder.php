<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role1 = Role::create([ 'name' => 'Root']);
        $role2 = Role::create([ 'name' => 'Administrador']);
        $role3 = Role::create([ 'name' => 'Usuario']);

        $root = User::create([
            'name' => 'Diego',
            'email' => 'root@root.com',
            'password' => bcrypt('root'),
        ]);

        $admin = User::create([
            'name' => 'Diego',
            'email' => 'admin@admin.com',
            'password' => bcrypt('1'),
        ]);

        $user = User::create([
            'name' => 'Leo',
            'email' => 'user@user.com',
            'password' => bcrypt('1'),
        ]);

        $root->assignRole($role1);

        $admin->assignRole($role2);

        $user->assignRole($role3);


        $permission = Permission::create(['name'=>'Ver usuarios']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Crear usuario']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Modificar usuario']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Eliminar usuario']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Ver roles']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Crear rol']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Modificar rol']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Eliminar rol']);
        $role1->givePermissionTo($permission);
        $permission = Permission::create(['name'=>'Asignar permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Ver permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Crear permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Modificar permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Eliminar permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Ver clientes']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Crear cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Modificar cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Eliminar cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Ver categorías']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Crear categoría']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Modificar categoría']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Eliminar categoría']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Ver rubros']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Crear rubro']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Modificar rubro']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Eliminar rubro']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Ver empresas']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Crear empresa']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Modificar empresa']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Eliminar empresa']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Ver cargos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Crear cargo']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Modificar cargo']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Eliminar cargo']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Ver tipos de clientes']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Crear tipo de cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Modificar tipo de cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Eliminar tipo de cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Ver parámetros']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Ver paises']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Crear país']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Modificar país']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Eliminar país']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Ver provincias']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Crear provincia']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Modificar provincia']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Eliminar provincia']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Ver ciudades']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Crear ciudad']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Modificar ciudad']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name'=>'Eliminar ciudad']);
        $role1->givePermissionTo($permission);


        $permission = Permission::create(['name'=>'Ver direcciones']);
        $role1->givePermissionTo($permission);
    }


}
