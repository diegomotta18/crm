<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable();
            $table->string('apellido')->nullable();
            $table->string('email')->unique();
            $table->date('fecha_de_nacimiento')->nullable();
            $table->string('tipo_cliente')->nullable();
            $table->string('sitio_web')->nullable();
            $table->string('telefono')->nullable();
            $table->string('celular')->nullable();
            $table->string('num_doc')->nullable();
            $table->enum('sexo',['Masculino','Femenino','Otro','Indicar'])->nullable();
            $table->string('interes')->nullable();
            $table->boolean('extranjero')->default(false);
            $table->integer('type_client_id')->unsigned()->nullable();
            $table->foreign('type_client_id')->references('id')
                ->on('type_clients')->onDelete('set null');
            $table->integer('address_id')->unsigned()->nullable();
            $table->foreign('address_id')->references('id')
                ->on('addresses')->onDelete('set null');
            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('id')
                ->on('companies')->onDelete('set null');
            $table->integer('position_id')->unsigned()->nullable();
            $table->foreign('position_id')->references('id')
                ->on('positions')->onDelete('set null');

            $table->boolean('status')->default(true);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('clients');
    }
}
