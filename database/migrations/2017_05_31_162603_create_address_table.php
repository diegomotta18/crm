<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('addresses', function(Blueprint $table){
            $table->increments('id');
            $table->bigInteger('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')
                ->on('countries')->onDelete('set null');
            $table->bigInteger('province_id')->unsigned()->nullable();
            $table->foreign('province_id')->references('id')
                ->on('provinces')->onDelete('set null');
            $table->bigInteger('district_id')->unsigned()->nullable();
            $table->foreign('district_id')->references('id')
                ->on('districts')->onDelete('set null');
            $table->bigInteger('place_id')->unsigned()->nullable();
            $table->foreign('place_id')->references('id')
                ->on('places')->onDelete('set null');
            $table->string('address')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('addresses');
    }
}
