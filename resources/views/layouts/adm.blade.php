<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    @include('includes.admin.plugins_admin_css')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CRM</title>
    <!--css plugins -->

</head>

<body ng-app="myapp">
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <nav class="navbar navbar-static-top" role="navigation"
                     style="border-color: #676a6c;margin-bottom: 0;border-style: solid;border-width:3px;">
                    <div class="navbar-header">
                        <h2 class="text-center" style="margin-left: 80px;">CRM</h2>
                    </div>
                </nav>
                <li class="nav-header">
                    <div class="dropdown profile-element text-center">
                            <span class="clear"> <strong
                                        class="font-bold "><i class="fa fa-user"
                                                              style="font-size: 48px;color:#FFFFFF;"></i></strong></span>
                        <a data-toggle="dropdown" class="dropdown-toggle " href="#">
                            <span class="clear">  <strong
                                        class="font-bold "> {{auth()->user()->name}} </strong>
                             </span> <span class="text-muted text-xs block"><i
                                        class="fa fa-tag"></i>  <b
                                ></b></span>
                            <br><br>
                        </a>
                    </div>
                </li>
                <li class="">
                    <a href="{{route('home')}}"><i class="fa fa-fw fa-home"></i> <span
                                class="nav-label">Inicio</span></a>
                </li>

                <li class="">
                    <a href="#">
                        <i class="fa fa-fw fa-users"></i>
                        <span class="nav-label">Clientes</span>
                        <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse" style="height:0px">
                        @can('Ver clientes')
                            <li><a href="{{route('clients.index')}}">Gestión de Clientes</a></li>
                        @endcan
                        <li><a href="{{route('export.index')}}">Filtrar y Exportar</a></li>
                    </ul>
                </li>
                @if(auth()->user()->can('Ver usuarios') || auth()->user()->can('Ver roles') || auth()->user()->can('Ver permisos'))
                    <li>
                        <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Seguridad</span> <span
                                    class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            @can('Ver usuarios')

                                <li><a href="{{route('users.index')}}"> <span
                                                class="nav-label">Usuarios</span></a></li>
                            @endcan
                            @can('Ver roles')
                                <li><a href="{{route('roles.index')}}"> <span
                                                class="nav-label">Roles</span></a></li>
                            @endcan
                            @role('Root')
                            <li><a href="{{route('permissions.index')}}"> <span
                                            class="nav-label">Permisos</span></a></li>
                            @endrole
                        </ul>
                    </li>
                @endif
                @can('Ver direcciones')
                    <li class="">
                        <a href="#">
                            <i class="fa fa-fw fa-map-marker"></i>
                            <span class="nav-label">Dirección</span>
                            <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse" style="height:0px">
                            @can('Ver paises')
                                <li><a href="{{route('countries.index')}}">Paises</a></li>
                            @endcan
                            @can('Ver provincias')
                                <li><a href="{{route('provinces.index')}}">Provincias</a></li>
                            @endcan
                            @can('Ver ciudades')
                                <li><a href="{{route('districts.index')}}">Ciudades</a></li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('Ver parámetros')

                    <li class="">
                        <a href="#">
                            <i class="fa fa-fw fa-cogs"></i>
                            <span class="nav-label">Parametros</span>
                            <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse" style="height:0px">
                            @can('Ver empresas')
                                <li><a href="{{route('companies.index')}}">Empresas</a></li>
                            @endcan
                            @can('Ver cargos')
                                <li><a href="{{route('positions.index')}}">Cargos</a></li>
                            @endcan
                            @can('Ver categorías')
                                <li><a href="{{route('categories.index')}}">Categorías</a></li>
                            @endcan
                            @can('Ver rubros')
                                <li><a href="{{route('items.index')}}">Rubros</a></li>
                            @endcan
                            @can('Ver tipos de clientes')
                                <li><a href="{{route('type_clients.index')}}">Tipo de cliente</a></li>
                            @endcan
                        </ul>
                    </li>
                @endcan

            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message"></span>
                    </li>
                    <li style="margin-right: 15px;"><a href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();"><i
                                    class="fa fa-fw fa fa-sign-out"></i>
                            Salir
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    @yield('content')

                </div>
            </div>
        </div>
    </div>
</div>

<!--Scripts plugins -->
@include('includes.admin.plugin_admin')
<!-- Scripts angular -->
@include('includes.admin.angular')
<!-- Scripts datatable-->
@include('includes.admin.datatable')
<!-- Scripts toast-->
@include('includes.toast')

</body>
</html>
