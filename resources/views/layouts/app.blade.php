<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    @include('includes.admin.plugins_admin_css')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>CRM</title>
    <!--css plugins -->
</head>
<body>
<div id="app">

    @yield('content')
</div>

<!-- Scripts -->
<!--Scripts plugins -->
@include('includes.admin.plugin_admin')
<!-- Scripts angular -->
@include('includes.admin.angular')
<!-- Scripts datatable-->
@include('includes.admin.datatable')
<!-- Scripts toast-->
@include('includes.toast')
</body>
</html>
