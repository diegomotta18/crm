@extends('layouts.adm')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Tipo de cliente</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i> <a href="{{route('home')}}">Principal</a>
                </li>
                <li class="active">
                    <i class="fa fa-list-ul"></i><strong> Tipo de cliente
                    </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <a href="{{route('type_clients.create')}}" class="btn btn-primary create"><i
                                    class="glyphicon glyphicon-plus"></i>Nuevo</a>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0"
                                   id="typeclients">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('parameters.type_clients.delete')
@endsection
