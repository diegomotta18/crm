@extends('layouts.adm')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Tipo de cliente</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-ul"></i> <a href="{{route('type_clients.index')}}">Tipo de cliente</a>
                </li>
                <li class="active">
                    <i class="fa fa-pencil-square-o"></i><strong> Modificar tipo de cliente
                    </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" align="center"><b>Datos del tipo de cliente</b></div>
                    <div class="ibox-content">
                        @include('partials/errors')
                        <form class="form" method="post" action="{{route('type_clients.update', $type_client->id)}}">
                            {!!csrf_field()!!}
                            {{ method_field('PUT')}}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Nombre</label>
                                        <div class="input-group">
                                            <input id="nombre" type="text" class="form-control" name="nombre" value="{{$type_client->nombre}}"
                                                   required>
                                            <span class="input-group-addon"><i class="fa fa-font "></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Descripción</label>
                                        <textarea id="nombre" type="text" class="form-control" name="descripcion" >{{$type_client->descripcion}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label class="checkbox-inline"><input type="checkbox" name="type_data_empresa" value="" @if($type_client->type_data == true) checked @endif>Admitir datos de empresa</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <button type="submit" id="btn_create" class="btn btn-primary"><i
                                                        class="fa fa-floppy-o"></i>
                                                Guardar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection