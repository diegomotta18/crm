@extends('layouts.adm')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Exportación de datos</h2>

            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i> <a href="{{route('home')}}">Principal</a>
                </li>
                <li class="active">
                    <i class="fa fa-list-ul"></i><strong> Exportación de datos
                    </strong>
                </li>
            </ol>

        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            @if(!is_null($clients))
                <div class="exports">

                    @include('renders.exports_table')
                </div>
            @endif
            <div class="col-md-3">
                @include('partials/errors')
                <form name="frmExport" id="frmExport" method="post" action="{{route('filter')}}" class="form">
                    {!! csrf_field() !!}
                    <div class="ibox float-e-margins well">
                        <div class="ibox-title" align="center"><b>Filtros</b></div>
                        <div class="ibox-content">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="checkbox" class="select_all" name="select_all" value="all"> Seleccionar todo<br>
                                    </div>
                                </div>
                            <div class="form-group">
                                <label class="control-label">Periodo</label>
                                <div class="input-group">
                                    <input type="text" id="fechas" name="periodo"
                                           value="{{old('periodo_de_participación')}}"
                                           class="form-control periodo"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                </div>
                            </div>

                            <div ng-controller="TypeClientCont">

                                <div class="form-group">
                                    <label class="control-label">Tipo de cliente</label>
                                    <div class="input-group m-b">
                                <span class="input-group-addon"><i
                                            class="fa fa-male"></i></span>
                                        <select class="form-control typeClient" name="tipo_de_cliente"
                                                id="tipo_cliente"
                                                ng-model="typeClient"
                                                ng-options="typeClient.nombre for typeClient in typeClients track by typeClient.id">
                                            <option value="" selected="">-- Seleccionar --</option>
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Sexo</label>
                                <div class="input-group m-b">
                                                <span class="input-group-addon"><i
                                                            class="fa  fa-transgender"></i></span>
                                    <select name="sexo" id="sexo"
                                            class="form-control bootstrap-select">
                                        <option value="" selected="">-- Seleccionar sexo--</option>
                                        <option value="Masculino">Masculino</option>
                                        <option value="Femenino">Femenino</option>
                                        <option value="">Ambos</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Nacionalidad</label>
                                <div class="input-group m-b">
                                <span class="input-group-addon"><i
                                            class="fa fa-language"></i></span>
                                    <select class="form-control" name="nacionalidad" id="nacionalidad">
                                        <option value="" selected="">-- Seleccionar --</option>
                                        <option value="false">Argentino</option>
                                        <option value="true">Extranjero</option>
                                    </select>
                                </div>
                            </div>
                            <div ng-controller="AddressPLCCont">
                                <div class="form-group">
                                    <label class="control-label">Provincia</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map"></i></span>
                                        <select id="" name="provincia"
                                                ng-init="getStates(1)"
                                                class="form-control bootstrap-select provincia"
                                                ng-model="state"
                                                ng-options="state.name for state in states track by state.id"
                                                ng-change="getDistricts(state.id)">
                                            <option value="" selected="">-- Seleccionar Provincia--</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Ciudad</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map"></i></span>
                                        <select id="ciu" name="ciudad"
                                                class="form-control bootstrap-select localidad"
                                                ng-model="district"
                                                ng-options="district.name for district in districts track by district.id"
                                                ng-change="getTowns(district.id)">
                                            <option value="" selected="">-- Seleccionar Ciudad--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <div class="btn-group ">
                                    <button id="btn_filter" type="button" class="btn btn-primary"><span><i
                                                    class="fa fa-filter"></i></span> Aplicar
                                    </button>
                                    <a href="{{route('exporting')}}"
                                       class="btn  btn-default pull-right"><i class="fa fa-file-excel-o"></i>
                                        Exportar
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
    </div>



@endsection