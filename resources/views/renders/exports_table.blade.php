<div id="loadexport">
    <div class="col-md-9">
        <div class="ibox float-e-margins well">
            <div class="ibox-title"><span>Resultados</span><span
                        class="pull-right">@if(!is_null($filters))@foreach($filters as $filter) <span
                            class="label label-info">{{ $filter }}</span> @endforeach @endif</span></div>
            <div class="ibox-title">
                <span><b>Total de contactos:</b> {{$clients->total()}} </span> @if($clients->total() > 0)<span
                        class="pull-right"><b>Visualizados:</b> {{$clients->firstItem()}}
                    de {{$clients->lastItem()}}</span>@endif
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="table-responsive">
                        <table id="" style="font-size: 12px;"
                               class="table table-striped table-bordered table-hover ">
                            <thead>
                            <tr>
                                <th class="text-center">Email</th>
                                <th class="text-center">Nº de Documento</th>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Apellido</th>
                                {{--<th class="text-center">Ciudad</th>--}}


                            </tr>
                            </thead>
                            <tbody class="text-center">

                            @foreach($clients as $client)
                                <tr>
                                    <td>{{ $client->email }}</td>
                                    <td>{{ $client->num_doc }}</td>
                                    <td>{{ $client->nombre }}</td>
                                    <td>{{ $client->apellido }}</td>
                                    {{--<td>@if(!is_null($client->address_id)){{ $client->address->district->name}} @endif </td>--}}

                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="row  form-inline">
                    <div class="loadexport">
                        {{ $clients->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
