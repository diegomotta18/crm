    @extends('layouts.adm')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Clientes</h2>

            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i> <a href="{{route('home')}}">Principal</a>
                </li>
                <li class="active">
                    <i class="fa fa-list-ul"></i><strong> Editar clientes <span
                                class="label label-primary">{{$typeclient->nombre}}</span>
                    </strong>
                </li>
            </ol>

        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">


        <div class="row">
            <div class="col-md-12">

                @include('partials/errors')
                <form id="frmClient" method="post" action="{{route('clients.updateWithoutCompany',$client->id)}}"
                      class="form">
                    {!! csrf_field() !!}
                    {{ method_field('PUT') }}
                    <div class="ibox float-e-margins">
                        <div class="ibox-title" align="center"><b>Datos personales</b></div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Nombre</label>
                                        <div class="input-group">
                                            <input id="nombre" name="nombre" type="text" class="form-control"
                                                   value="{{$client->nombre}}" required>
                                            <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Apellido</label>
                                        <div class="input-group">
                                            <input id="apellido" name="apellido" type="text" class="form-control"
                                                   value="{{$client->apellido}}" required>
                                            <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">N° Documento</label>
                                        <div class="input-group">
                                            <input type="number" id="num_doc" name="n°_documento"
                                                   ng-value="{{$client->num_doc}}" class="form-control" mask="99999999"
                                                   clean="false" ng-model="num_doc" required>
                                            <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                        </div>
                                    </div>


                                    <div class="form-group col-md-4">
                                        <label class="control-label">Fecha de Nacimiento</label>
                                        <div class="input-group">
                                            <input type="text" id="birthdate" name="fecha_de_nacimiento"
                                                   value="{{$client->fecha_de_nacimiento}}" class="form-control"/>
                                            <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Sexo</label>
                                        <div class="input-group">
                                            <select id="sorteos" name="sexo" id="sexo"
                                                    class="form-control bootstrap-select">
                                                <option value="">-- Seleccionar sexo--</option>
                                                <option value="Masculino"
                                                        @if($client->sexo == 'Masculino') selected @endif>
                                                    Masculino
                                                </option>
                                                <option value="Femenino"
                                                        @if($client->sexo == 'Femenino') selected @endif>
                                                    Femenino
                                                </option>
                                            </select>
                                            <span class="input-group-addon"><i class="fa fa-transgender"></i></span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Intereses</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                            <input id="interes" name="interes" type="text" class="form-control"
                                                   value="{{$client->interes}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox float-e-margins">

                        <div class="ibox-title" align="center"><b>Datos del domicilio</b></div>
                        <div class="ibox-content" id="app">
                            <div class="row">
                                @if(!is_null($address))

                                    <addresses country_id="{{$address->country_id}}" province_id="{{$address->province_id}}" district_id="{{$address->district_id}}"> </addresses>

                                    <div class="form-group col-md-12">
                                        <label class="control-label">Domicilio</label>
                                        <div class="input-group">
                                            <input id="domicilio" name="domicilio" type="text" class="form-control"
                                                   value="{{$address->address}}">
                                            <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                        </div>
                                    </div>

                                @else
                                    <addresses> </addresses>
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Domicilio</label>
                                        <div class="input-group">
                                            <input id="domicilio" name="domicilio" type="text" class="form-control"
                                                   value="">
                                            <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                        </div>
                                    </div>
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-title" align="center"><b>Datos del contacto</b></div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label class="control-label">Email</label>
                                    <div class="input-group">
                                        <input id="email" type="email" name="email" value="{{$client->email}}"
                                               class="form-control"
                                               ng-style="{'border-color': frmClient.email.$dirty&&frmClient.email.$error.pattern ? '#ff0000':''}"
                                               ng-pattern="/^([a-zA-Z0-9])+([a-zA-Z0-9._%+-])+@([a-zA-Z0-9_.-])+\.(([com]){3})$/"
                                               required>
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Telefono</label>
                                    <div class="input-group">
                                        <input id="telefono" name="telefono" type="number"
                                               value="{{$client->telefono}}"
                                               class="form-control" required>
                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Celular</label>
                                    <div class="input-group">
                                        <input id="celular" name="celular" type="number"
                                               value="{{$client->celular}}"
                                               class="form-control">
                                        <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection