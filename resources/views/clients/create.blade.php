@extends('layouts.adm')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Clientes</h2>

            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i> <a href="{{route('home')}}">Principal</a>
                </li>
                <li class="active">
                    <i class="fa fa-list-ul"></i><strong> Nuevo <span
                                class="label label-primary">{{$typeclient->nombre}}</span>
                    </strong>
                </li>
            </ol>

        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-md-12">

                @include('partials/errors')
                <form name="frmClient" id="frmClient" method="post" action="{{route('clients.store')}}" class="form">
                    {!! csrf_field() !!}
                    <div class="row">
                        <input type="hidden" name="tipo_de_cliente" value="{{$typeclient->id}}">

                        <div class="col-md-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title" align="center"><b>Datos personales</b></div>
                                <div class="ibox-content">
                                    <div class="row">

                                        <div class="form-group col-md-4">
                                            <input type="hidden" id="id_contact" value="">
                                            <label class="control-label">Nombre</label>
                                            <div class="input-group">
                                                <input id="nombre" name="nombre" type="text" class="form-control"
                                                       value="{{old('nombre')}}" required>
                                                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label">Apellido</label>
                                            <div class="input-group">
                                                <input id="apellido" name="apellido" type="text" class="form-control"
                                                       value="{{old('apellido')}}" required>
                                                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label">N° Documento</label>
                                            <div class="input-group">
                                                <input type="number" id="num_doc" name="n°_documento"
                                                       ng-value="{{old('n°_documento')}}" class="form-control"
                                                       mask="99999999"
                                                       clean="true" ng-model="num_doc" required>
                                                <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                            </div>
                                        </div>


                                        <div class="form-group col-md-4">
                                            <label class="control-label">Fecha de Nacimiento</label>
                                            <div class="input-group">
                                                <input type="text" id="birthdate" name="fecha_de_nacimiento"
                                                       value="{{old('fecha_de_nacimiento')}}" class="form-control"/>
                                                <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label">Sexo</label>
                                            <div class="input-group">
                                                <select id="sorteos" name="sexo" id="sexo"
                                                        class="form-control bootstrap-select">
                                                    <option value="" selected="">-- Seleccionar sexo--</option>
                                                    <option value="Masculino">Masculino</option>
                                                    <option value="Femenino">Femenino</option>
                                                </select>
                                                <span class="input-group-addon"><i
                                                            class="fa  fa-transgender"></i></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label">Intereses</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                                <input id="interes" name="interes" type="text" class="form-control"
                                                       value="{{old('interes')}}">
                                            </div>
                                        </div>

                                        <input type="hidden" id="edad" value="{{old('edad')}}" name="edad"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            @if($typeclient->id != 1 && $typeclient->id != 2)
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title" align="center"><b>Datos de la empresa</b></div>
                                    <div class="ibox-content">
                                        <div class="row">

                                            <div class="form-group col-md-4" ng-controller="CompanyCont">
                                                <label class="control-label">Empresa</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i
                                                                class="fa fa-building-o"></i></span>

                                                    <select class="form-control empresa" name="empresa"
                                                            ng-model="company"
                                                            ng-options="company.nombre for company in companies track by company.id">
                                                        <option value="" selected="">-- Seleccionar empresa--</option>
                                                    </select>


                                                    <div class="input-group-btn">
                                                        <a href="{{route('companies.index')}}" type="button"
                                                           class="btn btn-default">
                                                            <i class="glyphicon glyphicon-plus-sign"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4" ng-controller="CategoryCont">
                                                <label class="control-label">Categoria</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-bars"></i></span>

                                                    <select name="categoria"
                                                            class="form-control bootstrap-select categoria"

                                                            ng-model="category"
                                                            ng-options="category.nombre for category in categories track by category.id">
                                                        <option value="" selected="">-- Seleccionar categoria--</option>
                                                    </select>
                                                    <div class="input-group-btn">
                                                        <a href="{{route('categories.index')}}" type="button"
                                                           class="btn btn-default">
                                                            <i class="glyphicon glyphicon-plus-sign"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4" ng-controller="ItemCont">
                                                <label for="rubros" class="control-label">Rubro</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-bars"></i></span>

                                                    <select name="rubros[]" id="rubros" class="rubros form-control"
                                                            ng-model="item"
                                                            ng-options="item.nombre for item in items track by item.id"
                                                            multiple="multiple" style='height: 37px;'>


                                                    </select>

                                                    <div class="input-group-btn">
                                                        <a href="{{route('items.index')}}" type="button"
                                                           class="btn btn-default">
                                                            <i class="glyphicon glyphicon-plus-sign"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-4" ng-controller="PositionCont">
                                                <label class="control-label">Cargo</label>
                                                <div class="input-group">
                                                <span class="input-group-addon"><i
                                                            class="fa fa-graduation-cap"></i></span>

                                                    <select name="cargo" class="form-control bootstrap-select cargo"
                                                            ng-model="position"
                                                            ng-options="position.nombre for position in positions track by position.id">
                                                        <option value="" selected="">-- Seleccionar cargo--</option>
                                                    </select>
                                                    <div class="input-group-btn">
                                                        <a href="{{route('positions.index')}}" type="button"
                                                           class="btn btn-default">
                                                            <i class="glyphicon glyphicon-plus-sign"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label class="control-label">Sitio web</label>
                                                <div class="input-group">
                                                    <input type="text" placeholder="http://" id="sitio_web"
                                                           name="sitio_web"
                                                           value="{{old('sitio_web')}}" class="form-control"/>
                                                    <span class="input-group-addon"><i class="fa fa-slack"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            {{--</div>--}}

                            <div class="ibox float-e-margins"  >
                                <div class="ibox-title" align="center"><b>Datos del domicilio</b></div>
                                <div class="ibox-content">
                                    <div class="row" id="app">
                                        <addresses></addresses>
                                        {{--<div class="form-group col-md-3">--}}
                                            {{--<label class="control-label">Pais</label>--}}

                                            {{--<div class="input-group">--}}
                                                {{--<span class="input-group-addon"><i class="fa fa-map"></i></span>--}}
                                                {{--<select id="" name="pais"--}}
                                                        {{--class="form-control bootstrap-select country" ng-model="country"--}}
                                                        {{--ng-options="country.name for country in countries track by country.id"--}}
                                                        {{--ng-change="getStates(country.id)">--}}
                                                    {{--<option value="" selected="">-- Seleccionar Pais--</option>--}}
                                                {{--</select>--}}

                                            {{--</div>--}}
                                        {{--</div>--}}

                                        {{--<div class="form-group col-md-3">--}}
                                            {{--<label class="control-label">Provincia</label>--}}

                                            {{--<div class="input-group">--}}
                                                {{--<span class="input-group-addon"><i class="fa fa-map"></i></span>--}}
                                                {{--<select id="" name="provincia"--}}
                                                        {{--class="form-control bootstrap-select provincia" ng-model="state"--}}
                                                        {{--ng-options="state.name for state in states track by state.id"--}}
                                                        {{--ng-change="getDistricts(state.id)">--}}
                                                    {{--<option value="" selected="">-- Seleccionar Provincia--</option>--}}
                                                {{--</select>--}}

                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group col-md-3">--}}
                                            {{--<label class="control-label">Ciudad</label>--}}
                                            {{--<div class="input-group">--}}
                                                {{--<span class="input-group-addon"><i class="fa fa-map"></i></span>--}}
                                                {{--<select id="" name="ciudad"--}}
                                                        {{--class="form-control bootstrap-select localidad"--}}
                                                        {{--ng-model="district"--}}
                                                        {{--ng-options="district.name for district in districts track by district.id"--}}
                                                        {{--ng-change="getTowns(district.id)">--}}
                                                    {{--<option value="" selected="">-- Seleccionar Ciudad--</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group col-md-3">--}}

                                            {{--<label class="control-label">Barrio</label>--}}
                                            {{--<div class="input-group">--}}
                                                {{--<span class="input-group-addon"><i class="fa fa-map"></i></span>--}}
                                                {{--<select id="" name="localidad"--}}
                                                        {{--class="form-control bootstrap-select barrio"--}}
                                                        {{--ng-model="town"--}}
                                                        {{--ng-options="town.name for town in towns track by town.id">--}}
                                                    {{--<option value="" selected="">-- Seleccionar Barrio--</option>--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="form-group col-md-12">
                                            <label class="control-label">Domicilio</label>
                                            <div class="input-group">
                                                <input id="domicilio" name="domicilio" type="text" class="form-control"
                                                       value="{{old('domicilio')}}">
                                                <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="ibox float-e-margins">
                                <div class="ibox-title" align="center"><b>Datos del contacto</b></div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label class="control-label">Correo electrónico</label>
                                            <div class="input-group">
                                                <input id="email" type="text" value="{{old('email')}}" ng-model="email"
                                                       name="email" class="form-control"
                                                       ng-style="{'border-color': frmClient.email.$dirty&&frmClient.email.$error.pattern ? '#ff0000':''}"
                                                       ng-pattern="/^([a-zA-Z0-9])+([a-zA-Z0-9._%+-])+@([a-zA-Z0-9_.-])+\.(([com]){3})$/"
                                                       required>
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label">Telefono</label>
                                            <div class="input-group">
                                                <input id="telefono" name="telefono" type="number"
                                                       value="{{old('telefono')}}"
                                                       class="form-control" required>
                                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label class="control-label">Celular</label>
                                            <div class="input-group">
                                                <input id="celular" name="celular" type="number"
                                                       value="{{old('celular')}}"
                                                       class="form-control">
                                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <button type="submit" class="btn btn-primary pull-right">Aceptar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection
