<div class="form-group  col-md-12" ng-controller="ClientCont as mc">
    <div class="panel panel-primary well">
        <div class="panel-heading">Lista de clientes</div>
        <div class="panel-body">
            <div class="table-responsive">

                <table st-table="displayedClients" st-pipe="mc.callServer" class="table">
                    <thead>
                    <tr>
                        <th colspan="8"><input st-search="" class="form-control" placeholder="Búsqueda global..."
                                               type="text"/></th>
                    </tr>

                    <tr>

                        <th st-sort="nombre">Nombre</th>
                        <th st-sort="apellido">Apellido</th>
                        <th st-sort="email">Email</th>
                        <th st-sort="company">Empresa</th>
                        <th st-sort="position">Cargo</th>
                        <th st-sort="telefono">Teléfono</th>
                        <th st-sort="celular">Celular</th>
                        <th>Acciones</th>
                    </tr>

                    </thead>
                    <tbody ng-show="mc.isLoading">
                    <tr>
                        <td colspan="8" class="text-center">Cargando ...</td>
                    </tr>
                    <tbody>

                    <tr st-select-row="client" st-select-mode="multiple" ng-repeat="client in displayedClients">
                        <td>@{{client.nombre}}</td>
                        <td>@{{client.apellido}}</td>
                        <td>@{{client.email}}</td>
                        <td ng-if="client.empresa_nombre != null">@{{client.empresa_nombre}}</td>
                        <td ng-if="client.empresa_nombre == null" align="center">-</td>

                        <td ng-if="client.cargo != null" align="center">@{{client.cargo}}</td>

                        <td ng-if="client.cargo == null" align="center">-</td>

                        <td>@{{client.telefono}}</td>
                        <td>@{{client.celular}}</td>
                        <td width="100px;">
                            <div class="btn-group" role="group">

                                <a type="button" href="/clients/@{{ client.id }}" class="btn btn-xs btn-info">
                                    <i class="glyphicon glyphicon-eye-open">
                                    </i>
                                </a>
                                <a type="button" href="/clients/@{{ client.id }}/@{{client.tipo_cliente_id}}/edit"
                                   class="btn btn-xs btn-warning">
                                    <i class="glyphicon glyphicon-edit">
                                    </i>
                                </a>
                                @if (Auth::user()->role == 'god' or Auth::user()->role == 'admin' )
                                    <a type="button" class="btn btn-xs btn-danger"
                                       ng-bootbox-title="Eliminar cliente"
                                       ng-bootbox-confirm="Esta seguro de eliminar el cliente?"
                                       ng-bootbox-confirm-action="remove(client.id,client)">
                                        <i class="fa fa-trash blue"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                    </tbody>

                    <tfoot>
                    <tr ng-if="displayedClients.length > 0">
                        <td colspan="8">Mostrando registros del @{{  inicia}} al @{{ termina }} de un total
                            de @{{filteredCollection.length}} registros
                        </td>
                    </tr>
                    <tr>
                        @if (Auth::user()->role == 'god' or Auth::user()->role == 'admin' )
                            <td class="text-center" colspan="1" ng-if="displayedClients.length > 0">
                                <button ng-csv="filteredCollection" class="btn btn-primary pull-left" filename="crm.csv"
                                        style="margin-top: 20px;">Exportar a csv
                                </button>
                            </td>
                        @endif
                        <td class="text-center" st-pagination="" st-items-by-page="10" colspan="7">
                        </td>
                    </tr>
                    </tfoot>

                </table>

            </div>
        </div>
    </div>
</div>