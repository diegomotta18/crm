@extends('layouts.adm')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Clientes</h2>

            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i> <a href="{{route('home')}}">Principal</a>
                </li>
                <li class="active">
                    <i class="fa fa-list-ul"></i><strong> Nuevo @if(($typeclient)) <span
                                class="label label-primary">{{$typeclient->nombre}}</span> @endif
                    </strong>
                </li>
            </ol>

        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-md-12">
                <form id="frmClient" class="form-horizontal">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title" align="center"><b>Datos personales</b></div>
                        <div class="ibox-content">
                            <div class="row">
                                @if($client->nombre && $client->apellido)

                                    <div class="col-md-3">
                                        <strong><i class="fa   fa-user margin-r-5"></i> Nombre y apellido</strong>

                                        <p class="text-muted"> {{$client->nombre}}&nbsp&nbsp{{ $client->apellido}}</p>

                                        <hr>
                                    </div>
                                @endif
                                @if($client->num_doc)
                                    <div class="col-md-3">
                                        <strong><i class="fa   fa-hashtag margin-r-5"></i> N° Documento</strong>

                                        <p class="text-muted"> {{$client->num_doc}}</p>

                                        <hr>
                                    </div>
                                @endif

                                <div class="col-md-3">
                                    <strong><i class="fa   fa-birthday-cake margin-r-5"></i> Fecha de
                                        Nacimiento</strong>
                                    <p class="text-muted"> {{$client->fecha_de_nacimiento}}</p>
                                    <hr>
                                </div>
                                @if(!is_null($client->sexo))
                                    <div class="col-md-3">
                                        <strong><i class="fa   fa-transgender margin-r-5"></i> Sexo</strong>
                                        @if($client->sexo == 'Masculino')
                                            <p class="text-muted">Masculino</p>
                                        @endif
                                        @if($client->sexo == 'Femenino')
                                            <p class="text-muted"> Femenino</p>
                                        @endif
                                        <hr>
                                    </div>
                                @endif
                                @if($client->interes)
                                    <div class="col-md-3">
                                        <strong><i class="fa   fa-archive margin-r-5"></i> Intereses</strong>
                                        <p class="text-muted"> {{$client->interes}}</p>
                                        <hr>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if(!is_null($client->company_id))
                        <div class="ibox float-e-margins">
                            <div class="ibox-title" align="center"><b>Datos de la empresa</b></div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <strong><i class="fa   fa-building-o margin-r-5"></i> Empresa</strong>

                                        <p class="text-muted"> {{$company->nombre}}</p>

                                        <hr>
                                    </div>

                                    <div class="col-md-4">
                                        <strong><i class="fa   fa-bars margin-r-5"></i> Categoria</strong>

                                        <p class="text-muted"> {{$category->nombre}}</p>

                                        <hr>
                                    </div>

                                    <div class="col-md-4">
                                        <strong><i class="fa   fa-graduation-cap margin-r-5"></i> Cargo</strong>

                                        <p class="text-muted"> {{$position->nombre}}</p>

                                        <hr>
                                    </div>
                                    <div class="col-md-4">
                                        <strong><i class="fa   fa-slack margin-r-5"></i> Sitio web</strong>

                                        <p class="text-muted"> {{$client->sitio_web}}</p>

                                        <hr>
                                    </div>

                                    <div class="col-md-6" ng-controller="ItemCont">
                                        <label for="rubros" class="control-label"><i class="fa fa-bars"></i>
                                            Rubro</label>
                                        <div class="input-group">
                                            @foreach($rubros as $rubro)
                                                @if(in_array($rubro->id, $rubrosIds))
                                                    <span class="label label-primary">{{ $rubro->nombre }}</span>

                                                @endif
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($client->address_id)
                        <div class="ibox float-e-margins" ng-controller="AddressPLCCont">
                            <div class="ibox-title" align="center"><b>Datos del domicilio</b></div>
                            <div class="ibox-content">
                                <div class="row">

                                    @if(!is_null($country))
                                        <div class="col-md-3">
                                            <strong><i class="fa   fa-map margin-r-5"></i> País</strong>

                                            <p class="text-muted"> {{$country->name}}</p>

                                            <hr>
                                        </div>
                                    @endif
                                    @if(!is_null($provincia))
                                        <div class="col-md-3">
                                            <strong><i class="fa   fa-map margin-r-5"></i> Provincia</strong>
                                            <p class="text-muted"> {{$provincia->name}}</p>

                                            <hr>
                                        </div>
                                    @endif
                                    @if(!is_null($district))
                                        <div class="col-md-3">
                                            <strong><i class="fa   fa-map margin-r-5"></i> Departamento</strong>

                                            <p class="text-muted"> {{$district->name}}</p>

                                            <hr>
                                        </div>
                                    @endif
                                    @if(!is_null($place))

                                        <div class="col-md-3">
                                            <strong><i class="fa   fa-map margin-r-5"></i> Ciudad</strong>

                                            <p class="text-muted"> {{$place->name}}</p>

                                            <hr>
                                        </div>
                                    @endif
                                    @if(!is_null($address->address))

                                        <div class="col-md-3">
                                            <strong><i class="fa   fa-home margin-r-5"></i> Domicilio</strong>

                                            <p class="text-muted"> {{$address->address}}</p>

                                            <hr>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="ibox float-e-margins">
                        <div class="ibox-title" align="center"><b>Datos de contacto</b></div>
                        <div class="ibox-content">
                            <div class="row">

                                <div class="col-sm-6 col-md-4">
                                    <strong><i class="fa   fa-home margin-r-5"></i> Correo electrónica</strong>

                                    <p class="text-muted"> {{$client->email}}</p>

                                    <hr>
                                </div>
                                @if(!is_null($client->telefono))

                                    <div class="col-sm-6 col-md-4">
                                        <strong><i class="fa   fa-phone margin-r-5"></i> Telefono</strong>

                                        <p class="text-muted"> {{$client->telefono}}</p>

                                        <hr>
                                    </div>
                                @endif
                                @if(!is_null($client->celular))
                                    <div class="col-sm-6 col-md-4">
                                        <strong><i class="fa   fa-mobile margin-r-5"></i> Celular</strong>

                                        <p class="text-muted"> {{$client->celular}}</p>

                                        <hr>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection