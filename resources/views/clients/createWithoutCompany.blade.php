@extends('layouts.app')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Clientes</h2>

            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i> <a href="{{route('home')}}">Principal</a>
                </li>
                <li class="active">
                    <i class="fa fa-list-ul"></i><strong> Nuevo cliente <span
                                class="label label-primary">{{$typeclient->nombre}}</span>
                    </strong>
                </li>
            </ol>

        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            @include('partials/errors')
            <form name="frmClient" id="frmClient" method="post" action="{{route('clients.storeWithoutCompany')}}" class="form" >
                {!! csrf_field() !!}
                <div class="row">
                    <input type="hidden" name="tipo_de_cliente" value="{{$typeclient->id}}">
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Datos personales</h3>
                            </div>
                            <div class="panel-body">

                                <div class="form-group col-md-4">
                                    <input type="hidden" id="id_contact" value="">
                                    <label class="control-label">Nombre</label>
                                    <div class="input-group">
                                        <input id="nombre" name="nombre" type="text" class="form-control"
                                               value="{{old('nombre')}}" required>
                                        <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Apellido</label>
                                    <div class="input-group">
                                        <input id="apellido" name="apellido" type="text" class="form-control"
                                               value="{{old('apellido')}}" required>
                                        <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">N° Documento</label>
                                    <div class="input-group">
                                        <input type="number" id="num_doc" name="n°_documento"
                                               ng-value="{{old('n°_documento')}}" class="form-control" mask="99999999"
                                               clean="true" ng-model="num_doc" required>
                                        <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                    </div>
                                </div>


                                <div class="form-group col-md-4">
                                    <label class="control-label">Fecha de Nacimiento</label>
                                    <div class="input-group">
                                        <input type="text" id="birthdate" name="fecha_de_nacimiento"
                                               value="{{old('fecha_de_nacimiento')}}" class="form-control"/>
                                        <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Sexo</label>
                                    <div class="input-group">
                                        <select id="sorteos" name="sexo" id="sexo"
                                                class="form-control bootstrap-select">
                                            <option value="" selected="">-- Seleccionar sexo--</option>
                                            <option value="Masculino">Masculino</option>
                                            <option value="Femenino">Femenino</option>
                                        </select>
                                        <span class="input-group-addon"><i class="fa  fa-transgender"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Intereses</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                        <input id="interes" name="interes" type="text" class="form-control"
                                               value="{{old('interes')}}" >
                                    </div>
                                </div>

                                <input type="hidden" id="edad" value="{{old('edad')}}" name="edad"
                                       class="form-control"/>

                            </div>
                        </div>

                        <div class="panel panel-primary" ng-controller="AddressPLCCont">
                            <div class="panel-heading">
                                <h3 class="panel-title">Datos del domicilio</h3>

                            </div>
                            <div class="panel-body">


                                <div class="form-group col-md-3">
                                    <label class="control-label">Pais</label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map"></i></span>
                                        <select id="country" name="pais"
                                                class="form-control bootstrap-select country" ng-model="country"
                                                ng-options="country.country for country in countries track by country.code"
                                                ng-change="getProvinces(country.code)">
                                            <option value="" selected="">-- Seleccionar Pais--</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label class="control-label">Provincia</label>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map"></i></span>
                                        <select id="province" name="provincia"
                                                class="form-control bootstrap-select provincia" ng-model="province"
                                                ng-options="province.province for province in provinces track by province.code"
                                                ng-change="getCities(province.code)">
                                            <option value="" selected="">-- Seleccionar Provincia--</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label">Ciudad</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map"></i></span>
                                        <select id="district" name="ciudad"
                                                class="form-control bootstrap-select localidad" ng-model="city"
                                                ng-options="city.district for city in cities track by city.id"
                                                ng-change="getPlaces(city.id)">
                                            <option value="" selected="">-- Seleccionar Ciudad--</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">

                                    <label class="control-label">Barrio</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map"></i></span>
                                        <select id="place" name="localidad" class="form-control bootstrap-select barrio"
                                                ng-model="place"
                                                ng-options="place.place for place in places track by place.id">
                                            <option value="" selected="">-- Seleccionar Barrio--</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label">Domicilio</label>
                                    <div class="input-group">
                                        <input id="domicilio" name="domicilio" type="text" class="form-control"
                                               value="{{old('domicilio')}}" >
                                        <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Datos de contacto</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group col-md-4">
                                    <label class="control-label">Correo electrónico</label>
                                    <div class="input-group">
                                        <input id="email" type="text" value="{{old('email')}}" ng-model="email"
                                               name="email" class="form-control"
                                               ng-style="{'border-color': frmClient.email.$dirty&&frmClient.email.$error.pattern ? '#ff0000':''}"
                                               ng-pattern="/^([a-zA-Z0-9])+([a-zA-Z0-9._%+-])+@([a-zA-Z0-9_.-])+\.(([com]){3})$/"
                                               required>
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Telefono</label>
                                    <div class="input-group">
                                        <input id="telefono" name="telefono" type="number" value="{{old('telefono')}}"
                                               class="form-control" required>
                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Celular</label>
                                    <div class="input-group">
                                        <input id="celular" name="celular" type="number" value="{{old('celular')}}"
                                               class="form-control">
                                        <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-primary pull-right">Aceptar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
