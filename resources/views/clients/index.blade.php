@extends('layouts.adm')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Clientes</h2>

            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i> <a href="{{route('home')}}">Principal</a>
                </li>
                <li class="active">
                    <i class="fa fa-list-ul"></i><strong> Clientes
                    </strong>
                </li>
            </ol>

        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info" role="alert" align="justify">En esta pantalla usted podrá
                    visualizar una lista de todos los clientes, crear un nuevo <b>cliente</b> seleccionando un <b>tipo de cliente</b> o
                    editar los datos del cliente registrado. Además podra realizar busquedas por cualquier tipo
                    de filtro en el campo <b>Buscar</b>
                </div>
            </div>
            <div class="col-md-12" ng-controller="TypeClientCont" style="margin-top: -15px;">
                <div class="ibox float-e-margins well">
                    <div class="ibox-title">
                        <form class="form" method="GET" action="{{route('clients.create')}}">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <div class="input-group m-b"><span class="input-group-addon"><i
                                                class="fa fa-male"></i></span>
                                    <select class="form-control typeClient" name="tipo_de_cliente" id="tipo_cliente"
                                            ng-model="typeClient"
                                            ng-options="typeClient.nombre for typeClient in typeClients track by typeClient.id">
                                        <option value="" selected="">-- Seleccionar tipo de cliente--</option>
                                    </select>
                                    <span class="input-group-btn"><button type="submit" class="btn btn-primary"><i
                                                    class="glyphicon glyphicon-plus"></i> Nuevo</button></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="ibox float-e-margins well">

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table id="clients" style="font-size: 12px;"
                                   class="table table-striped table-bordered table-hover ">
                                <thead>
                                <tr>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Nº de Documento</th>
                                    <th class="text-center">Nombre</th>
                                    <th class="text-center">Apellido</th>
                                    <th class="text-center">Fecha de nacimiento</th>
                                    <th class="text-center">Ciudad</th>
                                    <th class="text-center">Fecha de inscripción</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                                </thead>
                                <tbody class="text-center">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="ibox float-e-margins well">
                    <div class="ibox-title" align="center"><b>Importación de clientes</b></div>
                    <div class="ibox-content">
                        @include('partials/errors')
                        <form method="POST" action="{{route('import')}}" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label"> <span class="fa fa-dot-circle-o"></span></label>
                                    <div class="fileinput fileinput-new input-group"
                                         data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput">
                                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                            <span class="fileinput-filename"></span>
                                        </div>
                                        <span class="input-group-addon btn btn-default btn-file">
                                            <span class="fileinput-new">Seleccionar archivo</span>
                                            <span class="fileinput-exists">Cambiar</span>
                                            <input type="file" name="archivo"/>
                                        </span>
                                        <a href="#"
                                           class="input-group-addon btn btn-default fileinput-exists"
                                           data-dismiss="fileinput">Eliminar</a>
                                    </div>

                                </div><!-- /input-group -->
                                <div class="input-group">
                                    <span class="input-group-btn">
                                 <button type="submit" class="btn btn-primary">Importar</button>
                                 </span>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('clients.delete')
@endsection