@extends('layouts.adm')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Clientes</h2>

            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i> <a href="{{route('home')}}">Principal</a>
                </li>
                <li class="active">
                    <i class="fa fa-list-ul"></i><strong> Editar clientes @if(!is_null($typeclient))<span
                                class="label label-primary">{{$typeclient->nombre}}</span>@endif
                    </strong>
                </li>
            </ol>

        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-md-12">

                @include('partials/errors')
                <form id="frmClient" method="post" action="{{route('clients.update',$client->id)}}" class="form">
                    {!! csrf_field() !!}
                    {{ method_field('PUT') }}

                    <div class="ibox float-e-margins">
                        <div class="ibox-title" align="center"><b>Tipo de cliente</b></div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group" ng-controller="TypeClientCont">
                                        <span class="input-group-addon"><i class="fa fa-male"></i></span>

                                        <select class="form-control typeClient" name="tipo_de_cliente" id="tipo_cliente"
                                                ng-model="typeClient"
                                                ng-options="typeClient.nombre for typeClient in typeClients track by typeClient.id">
                                            <option value="" selected="">-- Seleccionar tipo de cliente--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-title" align="center"><b>Datos personales </b></div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <input type="hidden" id="id_contact" value="">
                                    <label class="control-label">Nombre</label>
                                    <div class="input-group">
                                        <input id="nombre" name="nombre" type="text" class="form-control"
                                               value="{{$client->nombre}}" required>
                                        <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Apellido</label>
                                    <div class="input-group">
                                        <input id="apellido" name="apellido" type="text" class="form-control"
                                               value="{{$client->apellido}}" required>
                                        <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">N° Documento</label>
                                    <div class="input-group">
                                        <input type="number" id="num_doc" name="n°_documento"
                                               ng-value="{{$client->num_doc}}" class="form-control" mask="99999999"
                                               clean="false" ng-model="num_doc" required>
                                        <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                    </div>
                                </div>


                                <div class="form-group col-md-4">
                                    <label class="control-label">Fecha de Nacimiento</label>
                                    <div class="input-group">
                                        <input type="text" id="birthdate" name="fecha_de_nacimiento"
                                               value="{{$client->fecha_de_nacimiento}}" class="form-control"/>
                                        <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Sexo</label>
                                    <div class="input-group">
                                        <select id="sorteos" name="sexo" id="sexo"
                                                class="form-control bootstrap-select">
                                            <option value="">-- Seleccionar sexo--</option>
                                            <option value="Masculino"
                                                    @if($client->sexo == 'Masculino') selected @endif>
                                                Masculino
                                            </option>
                                            <option value="Femenino"
                                                    @if($client->sexo == 'Femenino') selected @endif>
                                                Femenino
                                            </option>
                                        </select>
                                        <span class="input-group-addon"><i class="fa fa-transgender"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Intereses</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                        <input id="interes" name="interes" type="text" class="form-control"
                                               value="{{$client->interes}}">
                                    </div>
                                </div>

                                <input type="hidden" id="edad" value="{{old('edad')}}" name="edad"
                                       class="form-control"/>

                            </div>
                        </div>
                    </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-title" align="center"><b>Datos de la empresa</b></div>
                        <div class="ibox-content">
                            <div class="row">
                                @if(!is_null($company))
                                    <div class="form-group col-md-4" ng-controller="CompanyCont">
                                        <label class="control-label">Empresa</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-building-o"></i></span>

                                            <select name="empresa" class="form-control bootstrap-select empresa"
                                                    ng-init="changeId({{$client->company_id}})" ng-model="company"
                                                    ng-options="company.nombre for company in companies track by company.id">
                                                <option value="" selected="">-- Seleccionar empresa--</option>
                                            </select>
                                            <div class="input-group-btn">
                                                <a href="{{route('companies.index')}}" type="button"
                                                   class="btn btn-default">
                                                    <i class="glyphicon glyphicon-plus-sign"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4" ng-controller="CategoryCont">
                                        <label class="control-label">Categoria</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>

                                            <select name="categoria" class="form-control bootstrap-select categoria"
                                                    ng-init="changeId({{$company->category_id}})"
                                                    ng-model="category"
                                                    ng-options="category.nombre for category in categories track by category.id">
                                                <option value="" selected="">-- Seleccionar categoria--</option>
                                            </select>
                                            <div class="input-group-btn">
                                                <a href="{{route('categories.index')}}" type="button"
                                                   class="btn btn-default">
                                                    <i class="glyphicon glyphicon-plus-sign"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4" ng-controller="ItemCont">
                                        <label for="rubros" class="control-label">Rubro</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>

                                            {{--<select name="rubros[]" id="rubros" class="rubros form-control"    ng-model="item" ng-options="item.nombre for item in items track by item.id" multiple="multiple" style='height: 35px;'>--}}


                                            {{--</select>--}}
                                            <select name="rubros[]" id="rubros" class="rubros form-control"
                                                    style='height: 35px;' multiple="multiple">
                                                @foreach($rubros as $rubro)
                                                    @if(in_array($rubro->id, $rubrosIds))
                                                        <option value="{{ $rubro->id }}"
                                                                selected="true">{{ $rubro->nombre }}</option>
                                                    @else
                                                        <option value="{{ $rubro->id }}">{{ $rubro->nombre }}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                            <div class="input-group-btn">
                                                <a href="{{route('items.index')}}" type="button"
                                                   class="btn btn-default">
                                                    <i class="glyphicon glyphicon-plus-sign"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4" ng-controller="PositionCont">
                                        <label class="control-label">Cargo</label>
                                        <div class="input-group">
                                                <span class="input-group-addon"><i
                                                            class="fa fa-graduation-cap"></i></span>

                                            <select name="cargo" class="form-control bootstrap-select cargo"
                                                    ng-init="changeId({{$client->position_id}})" ng-model="position"
                                                    ng-options="position.nombre  for position in positions track by position.id">
                                                <option value="" selected="">-- Seleccionar cargo--</option>
                                            </select>
                                            <div class="input-group-btn">
                                                <a href="{{route('positions.index')}}" type="button"
                                                   class="btn btn-default">
                                                    <i class="glyphicon glyphicon-plus-sign"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Sitio web</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="http://" id="sitio_web" name="sitio_web"
                                                   value="{{$client->sitio_web}}" class="form-control"/>
                                            <span class="input-group-addon"><i class="fa fa-slack"></i></span>
                                        </div>
                                    </div>

                                @else
                                    <div class="form-group col-md-4" ng-controller="CompanyCont">
                                        <label class="control-label">Empresa</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-building-o"></i></span>

                                            <select class="form-control empresa" name="empresa" ng-model="company"
                                                    ng-options="company.nombre for company in companies track by company.id">
                                                <option value="" selected="">-- Seleccionar empresa--</option>
                                            </select>


                                            <div class="input-group-btn">
                                                <a href="{{route('companies.index')}}" type="button"
                                                   class="btn btn-default">
                                                    <i class="glyphicon glyphicon-plus-sign"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4" ng-controller="CategoryCont">
                                        <label class="control-label">Categoria</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>

                                            <select name="categoria" class="form-control bootstrap-select categoria"

                                                    ng-model="category"
                                                    ng-options="category.nombre for category in categories track by category.id">
                                                <option value="" selected="">-- Seleccionar categoria--</option>
                                            </select>
                                            <div class="input-group-btn">
                                                <a href="{{route('categories.index')}}" type="button"
                                                   class="btn btn-default">
                                                    <i class="glyphicon glyphicon-plus-sign"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4" ng-controller="ItemCont">
                                        <label for="rubros" class="control-label">Rubro</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>

                                            <select name="rubros[]" id="rubros" class="rubros form-control"
                                                    ng-model="item"
                                                    ng-options="item.nombre for item in items track by item.id"
                                                    multiple="multiple" style='height: 37px;'>


                                            </select>

                                            <div class="input-group-btn">
                                                <a href="{{route('items.index')}}" type="button"
                                                   class="btn btn-default">
                                                    <i class="glyphicon glyphicon-plus-sign"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-4" ng-controller="PositionCont">
                                        <label class="control-label">Cargo</label>
                                        <div class="input-group">
                                                <span class="input-group-addon"><i
                                                            class="fa fa-graduation-cap"></i></span>

                                            <select name="cargo" class="form-control bootstrap-select cargo"
                                                    ng-model="position"
                                                    ng-options="position.nombre for position in positions track by position.id">
                                                <option value="" selected="">-- Seleccionar cargo--</option>
                                            </select>
                                            <div class="input-group-btn">
                                                <a href="{{route('positions.index')}}" type="button"
                                                   class="btn btn-default">
                                                    <i class="glyphicon glyphicon-plus-sign"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Sitio web</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="http://" id="sitio_web" name="sitio_web"
                                                   value="{{old('sitio_web')}}" class="form-control"/>
                                            <span class="input-group-addon"><i class="fa fa-slack"></i></span>
                                        </div>
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="ibox float-e-margins" id="app">
                        <div class="ibox-title" align="center"><b>Datos del domicilio </b></div>
                        <div class="ibox-content">
                            <div class="row">
                                @if($client->address)

                                    <addresses country_id="{{$address->country_id}}" province_id="{{$address->province_id}}" district_id="{{$address->district_id}}"> </addresses>

                                    <div class="form-group col-md-12">
                                        <label class="control-label">Domicilio</label>
                                        <div class="input-group">
                                            <input id="domicilio" name="domicilio" type="text" class="form-control"
                                                   value="{{$client->address->address}}">
                                            <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                        </div>
                                    </div>
                                @else

                                    <addresses></addresses>
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Domicilio</label>
                                        <div class="input-group">
                                            <input id="domicilio" name="domicilio" type="text" class="form-control"
                                                   value="{{old('domicilio')}}">
                                            <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title" align="center"><b>Datos de contacto</b></div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label class="control-label">Email</label>
                                    <div class="input-group">
                                        <input id="email" type="email" name="email" value="{{$client->email}}"
                                               class="form-control"
                                               ng-style="{'border-color': frmClient.email.$dirty&&frmClient.email.$error.pattern ? '#ff0000':''}"
                                               ng-pattern="/^([a-zA-Z0-9])+([a-zA-Z0-9._%+-])+@([a-zA-Z0-9_.-])+\.(([com]){3})$/"
                                               required>
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Telefono</label>
                                    <div class="input-group">
                                        <input id="telefono" name="telefono" type="number" value="{{$client->telefono}}"
                                               class="form-control" required>
                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Celular</label>
                                    <div class="input-group">
                                        <input id="celular" name="celular" type="number" value="{{$client->celular}}"
                                               class="form-control">
                                        <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection