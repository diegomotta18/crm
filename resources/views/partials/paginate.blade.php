@extends('layout')
@section('content')

<div class="container">
	<h1 class="page-header">Paginación</h1>
	<p>{{ $contacts->total() }} Contactos | {{ $contacts->currentPage()  }} Pagina de {{ $contacts->lastPage()  }} </p>

	<ul class="list-group">
		@foreach ($contacts as $contact)
		<li class="list-group-item">

		<span class="label label-info">{{$contact->grupo}}</span>
			{{$contact}} 
			{!! Form::open(['route'=>['delete',$contact->id],'method'=>'delete']) !!}
			<button type="submit" title="Eliminar">
				<i class="glyphicon glyphicon-trash"></i>
			</button>
			{!! Form::close() !!}
		</li>

		@endforeach
 	{!! $contacts->render() !!} 
</div>

@endsection