@extends('layouts.adm')

@section('content')
    <div class="container">
        <br>
        <div class="row ">
            <div class="col-md-8 col-md-offset-2 well ">
                <div class="panel panel-primary ">
                    <div class="panel-heading"><b>Bienvenido</b>, {{auth()->user()->name}}</div>
                </div>
            </div>
        </div>
    </div>
@endsection
