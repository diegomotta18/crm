@extends('layouts.adm')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Permisos</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i> <a href="{{route('home')}}">Principal</a>
                </li>
                <li class="active">
                    <i class="fa fa-list-ul"></i><strong> Permisos
                    </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">Lista de permisos
                    </div>

                    <div class="ibox-content">
                        <form class="form" method="post" action="{{route('roles.updpermissions',$rol->id)}}">
                            {!! csrf_field() !!}
                            {{ method_field('PUT') }}
                        @foreach ($permisos->chunk(4) as $chunk)
                            <div class="row">
                                @foreach ($chunk as $permiso)
                                    <div class="col-xs-3">
                                        <label class="checkbox-inline"><input type="checkbox" name="permisos[]" value="{{$permiso->name}}" @if($rol->hasPermissionTo($permiso->name )) checked @endif>{{ $permiso->name }}</label>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <button type="submit" id="btn_create" class="btn btn-primary"><i
                                                        class="fa fa-floppy-o"></i>
                                                Guardar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
