@extends('layouts.adm')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Provincias</h2>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-list-ul"></i> <a href="{{route('provinces.index')}}">Provincias</a>
                </li>
                <li class="active">
                    <i class="fa fa-pencil-square-o"></i><strong> Nueva provincia
                    </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <button onclick="javascript:window.history.back()" style="margin-top: 40px;"
                        class="btn  btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> Volver
                </button>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight" id="app">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" align="center"><b>Datos de la provincia</b></div>

                    <div class="ibox-content">
                        @include('partials/errors')

                        <form class="form" method="post" action="{{route('provinces.store')}}">
                            {!! csrf_field() !!}
                            <div class="row">
                                <country></country>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Nombre</label>
                                        <div class="input-group">
                                            <input  type="text" class="form-control" name="nombre" value=""
                                                   required>
                                            <span class="input-group-addon"><i class="fa fa-font "></i></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <button type="submit" id="btn_create" class="btn btn-primary"><i
                                                        class="fa fa-floppy-o"></i>
                                                Guardar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection