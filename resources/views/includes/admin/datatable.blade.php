
<script src="{{ asset('js/datatables/datatable_potcliente.js') }}"></script>
<script src="{{ asset('js/datatables/datatable_positions.js') }}"></script>
<script src="{{ asset('js/datatables/datatable_categories.js') }}"></script>
<script src="{{ asset('js/datatables/datatable_items.js') }}"></script>
<script src="{{ asset('js/datatables/datatable_companies.js') }}"></script>
<script src="{{ asset('js/datatables/datatable_users.js') }}"></script>
<script src="{{ asset('js/datatables/datatable_roles.js') }}"></script>
<script src="{{ asset('js/datatables/datatable_permissions.js') }}"></script>
<script src="{{ asset('js/datatables/datatable_typeclient.js') }}"></script>
<script src="{{ asset('js/datatables/datatable_countries.js') }}"></script>
