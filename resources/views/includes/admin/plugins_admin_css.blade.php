<link href="{{ asset('/admin/css/bootstrap.css')}}" rel="stylesheet">

<link href="{{ asset('/plugins/select2/select2.css')}}" rel="stylesheet">
<link href="{{ asset('/admin/css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
<link href="{{ asset('/admin/js/plugins/gritter/jquery.gritter.css')}}" rel="stylesheet">
<link href="{{ asset('/admin/css/animate.css')}}" rel="stylesheet">
<link href="{{ asset('/admin/css/jasny-bootstrap.min.css')}}" rel="stylesheet"/>

<link href="{{ asset('/admin/css/style.css')}}" rel="stylesheet">
<link href="{{ asset('/admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
<link href="{{ asset('/css/font.css') }}" rel="stylesheet">
{{--<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">--}}
<link href="{{ asset('/plugins/DataTables/DataTables-1.10.15/css/dataTables.bootstrap.css') }}" rel="stylesheet"/>
<link href="{{ asset('/plugins/DataTables/FixedHeader-3.1.2/css/fixedHeader.bootstrap.css') }}" rel="stylesheet"/>
<link href="{{ asset('/plugins/DataTables/Responsive-2.1.1/css/responsive.dataTables.css') }}" rel="stylesheet"/>
<link href="{{ asset('/plugins/DataTables/Responsive-2.1.1/css/responsive.bootstrap.css') }}" rel="stylesheet"/>
<link href="{{ asset('/plugins/daterangepicker/daterangepicker.css') }}" rel="stylesheet"/>
<link href="{{ asset('/plugins/select2/select2.css') }}"/>
