<script>
    @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    toastr.options.timeOut = 5000;
    toastr.options.closeButton = true;
    toastr.options.progressBar = true;
    toastr.options.showMethod = 'slideDown';
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
</script>