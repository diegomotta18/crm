@extends('errors::layout')

@section('title', 'Página no encontrada')

@section('message', 'La pagina que esta buscando no se encuentra')
