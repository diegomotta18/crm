@extends('errors::layout')

@section('title', 'Pagina expirada')

@section('message')
    La pagina ha expirado por inactividad
    <br/><br/>
@stop
