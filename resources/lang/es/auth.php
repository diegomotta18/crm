<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de inicio de sesión. Vuelva a intentarlo en :seconds segundos.',
    'remember_me' => 'Recordarme',
    'email_addres' => 'E-Mail',
    'password' => 'Contraseña',
    'forgot_your_password' => 'Olvidaste tu contraseña?',
    'login' => 'Ingresar',
    'login_title' => 'Ingreso a CRM',
    'register' => 'Registrar',
    'name' => 'Nombre',
    'confirm_password' => 'Confirmar contraseña',

];
