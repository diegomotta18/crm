<?php

namespace Tests\Browser;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $role = factory(Role::class)->create([
            'name' => 'Gestor',
        ]);

        $user = factory(User::class)->create([
            'email' => 'diegomotta18@gmail.com',
            'name' => 'Diego',
            'password' => bcrypt('root')
        ]);

        $user->assignRole($role);

//        $this->browse(function (Browser $browser) use ($user) {
//            $browser->loginAs($user)
//                ->visitRoute('users.index')
//                ->assertSee('Usuarios')
//                ->click('#nuevo-user')
//                ->assertRouteIs('users.create');
//        });

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/')
                ->type('email', $user->email)
                ->type('password', 'root')
                ->press('Ingresar')
                ->assertPathIs('/home');
        });
    }
}
