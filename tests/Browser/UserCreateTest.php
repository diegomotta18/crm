<?php

namespace Tests\Browser;

use App\Models\User;
use Illuminate\Http\Response;
use Spatie\Permission\Models\Role;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserCreateTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testCreateUser()
    {
        $user = $this->defaultUser();


        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visitRoute('users.index')
                ->click('#nuevo-user')
                ->assertRouteIs('users.create');
        });


    }


}
