<?php

/**
 * Created by PhpStorm.
 * User: diego
 * Date: 2/10/17
 * Time: 23:19
 */

namespace Tests;

use App\Models\Address;
use App\Models\Country;
use App\Models\District;
use App\Models\Person;
use App\Models\State;
use App\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

trait TestsHelper
{

    protected $defaultUser;


    public function defaultUser(array $attributes = [])
    {
        if ($this->defaultUser) {
            return $this->defaultUser;
        }

        $role1 = factory(Role::class)->create([
            'name' => 'Root',
        ]);
        $role2 = factory(Role::class)->create([
            'name' => 'Administrador',
        ]);
        $permission = Permission::create(['name' => 'Ver usuarios']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear usuario']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar usuario']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar usuario']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver roles']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear rol']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar rol']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar rol']);
        $role1->givePermissionTo($permission);
        $permission = Permission::create(['name' => 'Asignar permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver clientes']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver categorías']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear categoría']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar categoría']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar categoría']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver rubros']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear rubro']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar rubro']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar rubro']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver empresas']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear empresa']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar empresa']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar empresa']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver cargos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear cargo']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar cargo']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar cargo']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver tipos de clientes']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear tipo de cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar tipo de cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar tipo de cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver parámetros']);
        $role1->givePermissionTo($permission);

        $user = factory(\App\Models\User::class)->create([
            'email' => 'root@root.com',
            'name' => 'Root',
            'password' => bcrypt('root')
        ]);

        $user->assignRole($role1);


        return $this->defaultUser = $user;
    }

    public function createUser(array $attributes = [])
    {
        $user = factory(\App\Models\User::class)->create($attributes);
        return $user;
    }

    public function createRole(array $attributes =[]){
        $role1 = factory(Role::class)->create($attributes);

        $permission = Permission::create(['name' => 'Ver usuarios']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear usuario']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar usuario']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar usuario']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver roles']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear rol']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar rol']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar rol']);
        $role1->givePermissionTo($permission);
        $permission = Permission::create(['name' => 'Asignar permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar permisos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver clientes']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver categorías']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear categoría']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar categoría']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar categoría']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver rubros']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear rubro']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar rubro']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar rubro']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver empresas']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear empresa']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar empresa']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar empresa']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver cargos']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear cargo']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar cargo']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar cargo']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver tipos de clientes']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Crear tipo de cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Modificar tipo de cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Eliminar tipo de cliente']);
        $role1->givePermissionTo($permission);

        $permission = Permission::create(['name' => 'Ver parámetros']);
        $role1->givePermissionTo($permission);

        return $role1;
    }








//    protected function createPost(array $attributes = [])
//    {
//        return factory(\App\Post::class)->create($attributes);
//    }
}