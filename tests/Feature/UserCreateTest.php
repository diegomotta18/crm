<?php

namespace Tests\Feature;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserCreateTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateUser()
    {

        $this->actingAs($this->defaultUser());
        $reponse = $this->post(route('users.store'), [
            'nombre' => 'admin',
            'email' => 'admin@admin.com',
            'contraseña' => bcrypt('root'),
            'rol' => 'Administrador'
        ]);
        $usernew = User::whereEmail('admin@admin.com')->first();

        $reponse->assertStatus(302);
        $this->assertDatabaseHas('users',[
            'name' => 'admin',
            'email' => 'admin@admin.com',
        ]);
        $this->assertDatabaseHas('model_has_roles',[
            'model_id' => $usernew->id,
            'model_type' => 'App\Models\User',
            'role_id' => Role::whereName('Administrador')->first()->id
        ]);

    }


    public function testUpdateUser(){
        $this->actingAs($this->defaultUser());
        $user = $this->createUser(['name'=>'Admin','email'=> 'admin@admin.com', 'password'=> bcrypt('admin')]);
        $role = $this->createRole(["name"=>'Admin']);
        $user->assingRole($role);

        $reponse = $this->post(route('users.update',), [
            'nombre' => 'admin',
            'email' => 'admin@admin.com',
            'contraseña' => bcrypt('root'),
            'rol' => 'Administrador'
        ]);

    }
}
